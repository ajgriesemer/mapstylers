vm1 = new Vue({
  el: "#app",
  data: function () {
    return {
      destinations: [],
      results: [],
      targetSearch: null,
      target: null,
    };
  },
  methods: {
    deleteDirection: function (id) {
      for (let j = 0; j < this.destinations.length; j++) {
        const element = this.destinations[j];
        if (element.id == id) {
          this.destinations.splice(j, 1);
        }
      }
    },
    selectResult: function (result) {
      document.placesService.getDetails(
        {
          placeId: result.place_id,
          fields: ["geometry", "formatted_address"],
          sessionToken: document.sessionToken,
        },
        document.gotoSelectedResult
      );
    },
    deleteTarget: function () {
      vm1.$data.targetSearch = null;
      vm1.$data.target = null;
      document.targetMarker.setMap(null);
      document.targetMarker = null;
      vm1.$data.destinations = [];
    },
    toggleDirection: function (destination) {
      destination.inbound = !destination.inbound;
      for (let k = 0; k < document.directions.length; k++) {
        if (document.directions[k].id == destination.id) {
          document.directions[k].setMap(null);
          document.directions[k] = document.calculateDirections(destination);
        }
      }
    },
  },
  watch: {
    target: {
      handler: function (newdata, old) {
        if (newdata) {
          if (!document.targetMarker) {
            document.targetMarker = new google.maps.Marker({
              position: newdata.latLng,
              map: map,
              icon: newdata.icon,
              draggable: true,
              zIndex: google.maps.Marker.MAX_ZINDEX,
            });
            map.setCenter(newdata.latLng);
            vm1.$data.results = [];

            document.targetMarker.addListener("dragend", function (e) {
              vm1.$data.target.latLng = e.latLng;
              vm1.$data.target.label = e.latLng.lat().toFixed(5) + ", " + e.latLng.lng().toFixed(5);
              for (let i = 0; i < vm1.$data.destinations.length; i++) {
                for (let j = 0; j < document.directions.length; j++) {
                  if (vm1.$data.destinations[i].id == document.directions[j].id) {
                    document.directions[j].setMap(null);
                    document.directions[j] = document.calculateDirections(vm1.$data.destinations[i]);
                  }
                }
              }
              vm1.$data.destinations.forEach((dest) => {});
            });
          } else {
            document.targetMarker.setPosition(vm1.$data.target.latLng);
          }
        }
      },
      deep: true,
    },
    targetSearch: {
      handler: function (newdata, old) {
        if (newdata) {
          document.autocompleteService.getPlacePredictions(
            {
              input: newdata,
              sessionToken: document.sessionToken,
            },
            document.updateSearch
          );
        }
      },
      deep: true,
    },
    destinations: {
      handler: function (newdata, old) {
        for (let i = 0; i < newdata.length; i++) {
          // If document.directions doesn't exist yet create it
          if (!document.directions) {
            document.directions = [];
          }
          // If there is an existing directions for each new element in the new data
          if (document.directions[i]) {
            // Update the directions
            // If there is not an existing directions
          } else {
            // Create a new direction object for the Google Maps DirectionsRenderer object
            var newDirection = document.calculateDirections(newdata[i]);
            document.directions.push(newDirection);

            // This code is intended to space out the arrows as new arrows are drawn
            // for (let i = 0; i < document.directions.length; i++) {
            //   var offset =
            //     (i * (150 / document.directions.length)).toString() + "px";
            //   var correspondingDestination = newdata.find(d => {
            //     return document.directions[i].id == d.id;
            //   });
            //   if (correspondingDestination.inbound) {
            //     var options = inboundDirectionsRendererOptions;
            //   } else {
            //     var options = outboundDirectionsRendererOptions;
            //   }
            //   options.map = map;
            //   options.polylineOptions.icons[0].offset = offset;
            //   document.directions[i].setOptions(options);
            // }
          }
        }
        // If there are few directions in the new data than in the existing data, then a direction was removed
        if (document.directions.length > newdata.length) {
          // Iterate through the document.directions array and remove the elements that are no longer in newdata
          // Since elements are being removed from the array, iterate backwards
          for (let i = document.directions.length - 1; i >= 0; i--) {
            if (
              !newdata.find((d) => {
                return document.directions[i].id == d.id;
              })
            ) {
              document.directions[i].setMap(null);
              document.directions.splice(i, 1);
            }
          }
          if (document.directions.length > newdata.length) {
            for (let k = newdata.length; k < document.directions.length; k++) {
              const element = document.directions[k];
              element.setMap(null);
              document.directions.splice(k, 1);
            }
          }
        }
      },
      deep: true,
    },
  },
});

var map;
var start = "";
var end = "";

function initMap() {
  document.overlay = new google.maps.OverlayView();
  document.directionsService = new google.maps.DirectionsService();
  document.directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: true,
    polylineOptions: {
      strokeColor: "red",
    },
  });
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 39.167747, lng: -94.636855 },
    zoom: 15,
    styles: style,
    mapTypeId: "hybrid",
  });

  document.sessionToken = new google.maps.places.AutocompleteSessionToken();
  document.autocompleteService = new google.maps.places.AutocompleteService();
  document.placesService = new google.maps.places.PlacesService(map);

  document.directionsDisplay.setMap(map);

  map.addListener("click", function (e) {
    if (!vm1.$data.target) {
      vm1.$data.target = {
        id: Math.random(),
        latLng: e.latLng,
        icon: "../images/spotlight-poi2.png",
        label: e.latLng.lat().toFixed(5) + ", " + e.latLng.lng().toFixed(5),
      };
    } else {
      vm1.$data.destinations.push({
        id: Math.random(),
        latLng: e.latLng,
        label: e.latLng.lat().toFixed(5) + ", " + e.latLng.lng().toFixed(5),
        inbound: false,
      });
    }
  });
}

document.addEventListener("keyup", function (event) {
  if (event.keyCode == "80") {
    togglePrintView();
  }
});

function togglePrintView() {
  setPrintView(!document.printView);
}

function setPrintView(value) {
  document.printView = value;
  map.setOptions({
    zoomControl: !document.printView,
    streetViewControl: !document.printView,
    mapTypeControl: !document.printView,
    fullscreenControl: !document.printView,
  });
}

document.updateSearch = function (predictions, status) {
  if (status != google.maps.places.PlacesServiceStatus.OK) {
    alert(status);
    return;
  }
  vm1.$data.results = [];

  predictions.forEach(function (prediction) {
    vm1.$data.results.push(prediction);
  });
};

document.gotoSelectedResult = function (place, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    vm1.$data.target = {
      label: place.formatted_address,
      id: Math.random(),
      latLng: place.geometry.location,
      icon: {
        url: "../images/spotlight-poi2.png",
      },
    };
  }
};

document.calculateDirections = function (destination) {
  if (destination.inbound) {
    var request = {
      origin: destination.latLng,
      destination: vm1.$data.target.latLng,
      travelMode: "DRIVING",
    };
    var directionsRendererOptions = inboundDirectionsRendererOptions;
  } else {
    var request = {
      origin: vm1.$data.target.latLng,
      destination: destination.latLng,
      travelMode: "DRIVING",
    };
    var directionsRendererOptions = outboundDirectionsRendererOptions;
  }
  var newDirection = new google.maps.DirectionsRenderer(directionsRendererOptions);
  newDirection.setMap(map);

  document.directionsService.route(request, function (result, status) {
    if (status == "OK") {
      newDirection.setDirections(result);
      if (destination.inbound) {
        vm1.$data.target.latLng = newDirection.getDirections().routes[newDirection.getDirections().routes.length - 1].legs[
          newDirection.getDirections().routes[newDirection.getDirections().routes.length - 1].legs.length - 1
        ].end_location; //don't blame me, javascript sucks
      } else {
        vm1.$data.target.latLng = newDirection.getDirections().routes[0].legs[0].start_location;
      }
    }
  });
  newDirection.id = destination.id;

  newDirection.addListener("directions_changed", function (e) {
    document.directions.forEach((dir) => {
      var correspondingDestination = vm1.$data.destinations.find((dest) => {
        return dir.id == dest.id;
      });
      if (correspondingDestination.inbound) {
        correspondingDestination.latLng = dir.getDirections().routes[0].legs[0].start_location;
      } else {
        correspondingDestination.latLng = dir.getDirections().routes[newDirection.getDirections().routes.length - 1].legs[
          newDirection.getDirections().routes[newDirection.getDirections().routes.length - 1].legs.length - 1
        ].end_location; //don't blame me, javascript sucks
      }

      correspondingDestination.label = correspondingDestination.latLng.lat().toFixed(5) + ", " + correspondingDestination.latLng.lng().toFixed(5);
    });
  });
  return newDirection;
};

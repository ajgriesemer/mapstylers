var style = [
  {
    featureType: "poi",
    stylers: [
      {
        visibility: "off"
      }
    ]
  }
];

var outboundDirectionsRendererOptions = {
  draggable: true,
  preserveViewport: true,
  markerOptions: {
    icon: "../images/spotlight-poi2.png"
  },
  polylineOptions: {
    strokeColor: "#0080ff",
    strokeWeight: 6,
    strokeOpacity: 0.55,
    fillOpacity: 0,
    icons: [
      {
        icon: {
          path: "M -2.1,3.5 0,-1 2.1,3.5",
          strokeOpacity: 0.55,
          strokeWeight: 6,
          scale: 4
        },
        offset: "50px",
        repeat: "150px"
      }
    ]
  }
};

var inboundDirectionsRendererOptions = {
  draggable: true,
  preserveViewport: true,
  markerOptions: {
    icon: "../images/spotlight-poi2.png"
  },
  polylineOptions: {
    strokeColor: "#FF2613",
    strokeWeight: 6,
    strokeOpacity: 0.55,
    fillOpacity: 0,
    icons: [
      {
        icon: {
          path: "M -2.1,3.5 0,-1 2.1,3.5",
          strokeOpacity: 0.55,
          strokeWeight: 6,
          scale: 4
        },
        offset: "0px",
        repeat: "150px"
      }
    ]
  }
};

var directionsRendererOptions = [
  {
    draggable: true,
    preserveViewport: true,
    markerOptions: {
      icon: "../images/spotlight-poi2.png"
    },
    polylineOptions: {
      strokeColor: "#0080ff",
      strokeWeight: 6,
      strokeOpacity: 0.55,
      fillOpacity: 0,
      icons: [
        {
          icon: {
            path: "M -2.1,3.5 0,-1 2.1,3.5",
            strokeOpacity: 0.55,
            strokeWeight: 6,
            scale: 4
          },
          offset: "50px",
          repeat: "150px"
        }
      ]
    }
  },
  {
    draggable: true,
    preserveViewport: true,
    markerOptions: {
      icon: "../images/spotlight-poi2.png"
    },
    polylineOptions: {
      strokeColor: "#0080ff",
      strokeWeight: 6,
      strokeOpacity: 0.55,
      fillOpacity: 0,
      icons: [
        {
          icon: {
            path: "M -2.1,3.5 0,-1 2.1,3.5",
            strokeOpacity: 0.55,
            strokeWeight: 6,
            scale: 4
          },
          offset: "125px",
          repeat: "150px"
        }
      ]
    }
  },
  {
    draggable: true,
    preserveViewport: true,
    markerOptions: {
      icon: "../images/spotlight-poi2.png"
    },
    polylineOptions: {
      strokeColor: "#FF2613",
      strokeWeight: 6,
      strokeOpacity: 0.55,
      fillOpacity: 0,
      icons: [
        {
          icon: {
            path: "M -2.1,3.5 0,-1 2.1,3.5",
            strokeOpacity: 0.55,
            strokeWeight: 6,
            scale: 4
          },
          offset: "0px",
          repeat: "150px"
        }
      ]
    }
  },
  {
    draggable: true,
    preserveViewport: true,
    markerOptions: {
      icon: "../images/spotlight-poi2.png"
    },
    polylineOptions: {
      strokeColor: "#FF2613",
      strokeWeight: 6,
      strokeOpacity: 0.55,
      fillOpacity: 0,
      icons: [
        {
          icon: {
            path: "M -2.1,3.5 0,-1 2.1,3.5",
            strokeOpacity: 0.55,
            strokeWeight: 6,
            scale: 4
          },
          offset: "75px",
          repeat: "150px"
        }
      ]
    }
  }
];
// var baseDirectionsRendererOption = ;

// directionsRendererOptions.push(baseDirectionsRendererOption);

// var secondDirectionsRendererOption = baseDirectionsRendererOption;
// secondDirectionsRendererOption.polylineOptions.strokeColor = "#00ff00";
// secondDirectionsRendererOption.polylineOptions.icons[0].offset = "35px";
// secondDirectionsRendererOption.polylineOptions.icons[1].offset = "35px";

// directionsRendererOptions.push(secondDirectionsRendererOption);
// directionsRendererOptions.push(baseDirectionsRendererOption);
// directionsRendererOptions.push(baseDirectionsRendererOption);

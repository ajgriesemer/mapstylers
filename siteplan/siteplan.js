////////////////////////////////////////////////////
// SVG icons that are used for the placemarker handles
////////////////////////////////////////////////////
// Square used for scale placemarker handle
const square = {
  path: 'M  -5, -5 L -5,5 L 5,5 L 5,-5 L -5,-5',
  fillOpacity: 1,
  fillColor: 'white',
  strokeWeight: 1,
  scale: 1,
};
// A larger circle that is used for the rotate hit area, is typically transparent
const rotatorCircle = {
  path: 'M 0, 0 m -200, 0 a 200,200 0 2,0 400,0 a 200,200 0 2,0 -400,0',
  fillOpacity: 0,
  strokeWeight: 2,
  scale: 0.1,
};

////////////////////////////////////////////////////
// initMap is a callback function called by the Google Maps API after the API script is loaded
////////////////////////////////////////////////////
function initMap() {
  // Set the styling and initial center of the map
  const mapOptions = {
    tilt: 0,
    zoom: 17,
    center: new google.maps.LatLng(39.09696305509259, -94.73900612960772),
    mapTypeId: 'hybrid',
    styles: [
      {
        featureType: 'poi',
        stylers: [{visibility: 'off'}],
      },
      {
        featureType: 'transit',
        elementType: 'labels.icon',
        stylers: [{visibility: 'off'}],
      },
    ],
  };
  // Create map
  const map = new google.maps.Map(document.getElementById('map'), mapOptions);


  // Wait for the map to trigger the 'idle' event to complete the initialization
  // This is probably not the best place for this initializiation to live, it would be
  // better in the equivalent of the jquery ready function
  google.maps.event.addListenerOnce(map, 'idle', () => {

    const centerControlDiv = document.createElement('div');

    // Set CSS for the control border.
    const controlLeft = document.createElement('div');
    controlLeft.style.backgroundColor = '#fff';
    controlLeft.style.border = '2px solid #fff';
    controlLeft.style.borderRadius = '3px 0 0 3px';
    controlLeft.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlLeft.style.cursor = 'pointer';
    controlLeft.style.marginBottom = '22px';
    controlLeft.style.marginTop = '10px';
    controlLeft.style.textAlign = 'center';
    controlLeft.style.float = 'left';

    const controlRight = document.createElement('div');
    controlRight.style.backgroundColor = '#fff';
    controlRight.style.border = '2px solid #fff';
    controlRight.style.borderRadius = '0 3px 3px 0';
    controlRight.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlRight.style.cursor = 'pointer';
    controlRight.style.marginBottom = '22px';
    controlRight.style.marginTop = '10px';
    controlRight.style.textAlign = 'center';
    controlRight.style.float = 'right';
    centerControlDiv.appendChild(controlRight);
    centerControlDiv.appendChild(controlLeft);
  
    // Set CSS for the control interior.
    const controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Add Cross Dock';

    const controlTextRight = document.createElement('div');
    controlTextRight.style.color = 'rgb(25,25,25)';
    controlTextRight.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlTextRight.style.fontSize = '16px';
    controlTextRight.style.lineHeight = '38px';
    controlTextRight.style.paddingLeft = '5px';
    controlTextRight.style.paddingRight = '5px';
    controlTextRight.innerHTML = 'Add Single Load';
    document.controlText = controlText.innerHTML;
    document.controlTextRight = controlTextRight.innerHTML;
    controlLeft.appendChild(controlText);
    controlRight.appendChild(controlTextRight);
  
    controlLeft.addEventListener('click', () => {
      buildingCount += 1;
      new SitePlanOverlay(map, map.getCenter(), 11, 29, buildingCount, true);
    });
    controlRight.addEventListener('click', () => {
      buildingCount += 1;
      new SitePlanOverlay(map, map.getCenter(), 5, 20, buildingCount, false);
    });
  
  
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
  


    ////////////////////////////////////////////////////
    // The SitePlanOverlay class contains all of the functionality for the
    // polygons and placemarkers for an editable
    ////////////////////////////////////////////////////
    class SitePlanOverlay extends google.maps.OverlayView {
      constructor(map, center, width, length, number, crossDock) {
        super();
        this.map_ = map;
        this.center_ = center; // the center of the building
        this.width_ = width; // width of the building in # of bays
        this.length_ = length; // length of the building in # of bays
        this.angle_ = 0; // starting rotation of the building
        if(crossDock){
          this.bayWidth = 50; // width of each bay
          this.bayLength = 52; // length of each bay
          this.speedbayWidth = 60; // width of the speed bay (2 of these for a cross dock)
          this.truckCourtDepth = 190; // width of the trailer areas
          this.minWidth = 8; // minimum number of bays along the width
          this.minLength = 14; // minimum number of bays along the length
          this.maxWidth = 12; // maximum number of bays along the width
          this.maxLength = 63; // maximum number of bays along the length
        } else { // single load
          this.bayWidth = 52; // width of each bay
          this.bayLength = 52; // length of each bay
          this.speedbayWidth = 60; // width of the speed bay (2 of these for a cross dock)
          this.truckCourtDepth = 190; // width of the trailer areas
          this.minWidth = 5; // minimum number of bays along the width
          this.minLength = 10; // minimum number of bays along the length
          this.maxWidth = 6; // maximum number of bays along the width
          this.maxLength = 30; // maximum number of bays along the length
        }

        this.parkingOffset = 20; // distance between the car parking and the building
        this.parkingDepth = 65; // depth of the car parking
        this.crossDock = crossDock
        this.number = number;
        this.setMap(map); // Assign the map to the overlay view
      }

      onAdd() {
        // Create the building polygon
        this.building = new google.maps.Polygon({
          map,
          paths: this.getPath(),
          draggable: true,
          fillColor: '#FF8000',
          fillOpacity: 0.5,
          strokeWeight: 1,
        });
        // Create each of the parking lot polygons
        this.parkingLots_ = this.getParkingPaths().map((p, i) => {
          const polygon = new google.maps.Polygon({
            map,
            paths: p,
            fillColor: '#323232',
            fillOpacity: 0.5,
            strokeWeight: 1,
          });
          return polygon;
        });
        // Create each of the placemarker handles at the center of each edge
        this.edges_ = this.getEdges().map((c, i) => {
          const marker = new google.maps.Marker({
            position: c,
            map,
            draggable: true,
            icon: square,
            zIndex: 2,
          });
          // Both the drag and dragend events trigger the same edgeScale method
          google.maps.event.addListener(marker, 'drag', (e) => {
            this.edgeScale(e, i);
          });
          google.maps.event.addListener(marker, 'dragend', (e) => {
            this.edgeScale(e, i);
          });
          return marker;
        });
        // Create the placemarker that makes a clickable area for rotating at each corner
        this.rotators_ = this.getCorners().map((c, i) => {
          const coloredCircle = rotatorCircle;
          coloredCircle.strokeColor = '#FF00ff00';
          const marker = new google.maps.Marker({
            position: c,
            map,
            draggable: true,
            icon: coloredCircle,
            zIndex: 1,
            cursor: 'url(\'../images/se-rotate.png\'), auto',
          });
          // Both the drag and dragend events trigger the same rotate method
          google.maps.event.addListener(marker, 'drag', (e) => {
            this.rotate(e, i);
          });
          google.maps.event.addListener(marker, 'dragend', (e) => {
            this.rotate(e, i);
          });
          return marker;
        });

        // Update the building location when the building is moved
        google.maps.event.addListener(this.building, 'drag', (e) => {
          this.drag(e);
        });

        // Some weird things happen to the building sizing if we update on a draw function
        // in the middle of a zoom.
        // So set a flag during the zoom so we wait to update the building size until
        // the draw function is called when the zoom is complete
        map.addListener('zoom_changed', () => {
          this.zoomInProgress = true;
          console.log('zoom_changed');
        });

        map.addListener('idle', () => {
          this.zoomInProgress = false;
          console.log('idle');
        });

        // Create and style the text overlay
        const projection = this.getProjection();
        const center = this.center_;
        const screenCenter = projection.fromLatLngToDivPixel(center);
        const halfWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth);
        const halfLength = this.feetToHorizontalPixels(this.length_ * this.bayLength / 2);
        this.textOverlay = document.createElement('div');
        this.textOverlay.style.transform = `rotate(${this.angle_}deg)`;
        this.textOverlay.style.position = 'absolute';
        this.textOverlay.style.left = `${screenCenter.x - halfLength}px`;
        this.textOverlay.style.top = `${screenCenter.y - halfWidth}px`;
        this.textOverlay.style.width = `${2 * halfLength}px`;
        this.textOverlay.style.height = `${2 * halfWidth}px`;
        this.textOverlay.style.display = 'flex';
        this.textOverlay.style.alignItems = 'center';
        this.textOverlay.style.justifyContent = 'center';
        this.textOverlay.style.lineHeight = 'normal';
        this.textOverlay.style.textAlign = 'center';
        this.textOverlay.style.fontFamily = 'Roboto';
        this.textOverlay.style.fontSize = '3em';
        this.textOverlay.style.zIndex = '100';
        this.textOverlay.innerHTML = 'Building ' + this.number + ' <br> 520\' x  1280\'';

        // Add the text overlay to the same pane as the polygons
        const panes = this.getPanes();
        panes.overlayLayer.appendChild(this.textOverlay);
      }

      draw() {
        // Update the text location
        const projection = this.getProjection();
        const screenCenter = projection.fromLatLngToDivPixel(this.center_);
        const halfWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth);
        const halfLength = this.feetToHorizontalPixels(this.length_ * this.bayLength / 2);
        this.textOverlay.style.transform = `rotate(${this.angle_}rad)`;
        this.textOverlay.style.left = `${screenCenter.x - halfLength}px`;
        this.textOverlay.style.top = `${screenCenter.y - halfWidth}px`;
        this.textOverlay.style.width = `${2 * halfLength}px`;
        this.textOverlay.style.height = `${2 * halfWidth}px`;
        this.textOverlay.innerHTML = this.getBuildingString();

        // Update the font size based on the zoom level.
        // getWorldWidth was chosen because it updates consistently throughout the zoom animation
        // 1382400 is a magic number derived from testing
        this.textOverlay.style.fontSize = `${projection.getWorldWidth() / 1382400}px`;
      }

      // Helper function to provide a formatted string with the building number, dimensions and size
      getBuildingString() {
        const line1 = 'Building ' + this.number + ' <br>';
        const line2 = `${this.getWidth()}' x  ${this.getLength()}' <br>`;
        const line3 = `${(Math.floor(this.getWidth() * this.getLength()/1000) * 1000)
                        .toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')} sf`;
        return line1 + line2 + line3;
      }

      // Helper function to calculate the length of the building
      getLength() {
        return this.length_ * this.bayLength;
      }

      // Helper function to calculate the width of the building
      getWidth() {
        return (this.width_ - 2) * this.bayWidth + 2 * this.speedbayWidth;
      }

      // Convert from feet in the world to pixel distance on the screen
      // Because of spherical coordinates, the horizontal and vertical distance per pixel is different
      horizontalPixelsToFeet(feet) {
        const mapBounds = this.map_.getBounds();
        const worldWidth = this.horizontalMapDistance(mapBounds);
        const screenWidth = document.getElementById('map').offsetWidth;
        return feet * worldWidth / screenWidth;
      }

      // Convert from feet in the world to pixel distance on the screen
      // Because of spherical coordinates, the horizontal and vertical distance per pixel is different
      verticalPixelsToFeet(feet) {
        const mapBounds = map.getBounds();
        const worldHeight = this.verticalMapDistance(mapBounds);
        const screenHeight = document.getElementById('map').offsetHeight;
        return feet * worldHeight / screenHeight;
      }

      // Convert from pixel distance on the screen to feet in the world
      // Because of spherical coordinates, the horizontal and vertical distance per pixel is different
      feetToHorizontalPixels(feet) {
        const mapBounds = this.map_.getBounds();
        const worldWidth = this.horizontalMapDistance(mapBounds);
        const screenWidth = document.getElementById('map').offsetWidth;
        return feet * screenWidth / worldWidth;
      }

      // Convert from pixel distance on the screen to feet in the world
      // Because of spherical coordinates, the horizontal and vertical distance per pixel is different
      feetToVerticalPixels(feet) {
        const mapBounds = map.getBounds();
        const worldHeight = this.verticalMapDistance(mapBounds);
        const screenHeight = document.getElementById('map').offsetHeight;
        return feet * screenHeight / worldHeight;
      }

      // Calculate the distance in feet across the map horizontally
      horizontalMapDistance(bounds) {
        const lat1 = bounds.getNorthEast().lat();
        const lat2 = bounds.getNorthEast().lat();
        const lng1 = bounds.getNorthEast().lng();
        const lng2 = bounds.getSouthWest().lng();
        return this.haversine(lat1, lng1, lat2, lng2);
      }

      // Calculate the distance in feet across the map vertically
      verticalMapDistance(bounds) {
        const lat1 = bounds.getNorthEast().lat();
        const lat2 = bounds.getSouthWest().lat();
        const lng1 = bounds.getNorthEast().lng();
        const lng2 = bounds.getNorthEast().lng();
        return this.haversine(lat1, lng1, lat2, lng2);
      }

      // A standard mathematical equation for calculating distance based on two spherical coordinates
      // Returns distance in feet
      haversine(lat1, lng1, lat2, lng2) {
        const R = 6378.137;
        const dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
        const dLon = lng2 * Math.PI / 180 - lng1 * Math.PI / 180;
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c;
        return d * 3280.84;
      }

      // Get the locations for each of the edge placemarkers
      getEdges() {
        const corners = this.getCorners();
        const edges = [];
        for (let i = 0; i < corners.length; i++) {
          edges[i] = new google.maps.LatLng(
              (corners[i].lat() + corners[(i + 1) % 4].lat()) / 2,
              (corners[i].lng() + corners[(i + 1) % 4].lng()) / 2,
          );
        }
        return edges;
      }

      // Get the location of each corner, currently the same as getPath,
      // but eventually the building path should show horizontal articulation, so this may be different
      getCorners() {
        return this.getPath();
      }

      // Get the outline of the building
      getPath() {
        const center = this.center_;
        const projection = this.getProjection();
        const screenCenter = projection.fromLatLngToContainerPixel(center);

        var halfLength, halfWidth;

        if(this.crossDock){
          halfLength = this.feetToHorizontalPixels(this.length_ * this.bayLength / 2);
          halfWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth);
        } else {
          halfLength = this.feetToHorizontalPixels(this.length_ * this.bayLength / 2);
          halfWidth = this.feetToVerticalPixels(((this.width_ - 1) * this.bayWidth + this.speedbayWidth ) / 2);
        }

        const sw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, halfWidth, -halfLength));
        const nw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidth, -halfLength));
        const ne = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidth, halfLength));
        const se = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, halfWidth, halfLength));

        return [sw, nw, ne, se];
      }

      // Get the outline of each parking lot
      // Returns an array of paths, one for each parking lot
      getParkingPaths() {
        const center = this.center_;
        const projection = this.getProjection();
        const screenCenter = projection.fromLatLngToContainerPixel(center);
        
        if(this.crossDock)
        {
          const halfLengthMinusOneBay = this.feetToHorizontalPixels((this.length_ * this.bayLength) / 2 - this.bayLength);
          const nearWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth);
          const farWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth + this.truckCourtDepth);
          let sw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, nearWidth, -halfLengthMinusOneBay));
          let nw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, farWidth, -halfLengthMinusOneBay));
          let ne = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, farWidth, halfLengthMinusOneBay));
          let se = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, nearWidth, halfLengthMinusOneBay));

          const top = [sw, nw, ne, se];

          sw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -nearWidth, -halfLengthMinusOneBay));
          nw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -farWidth, -halfLengthMinusOneBay));
          ne = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -farWidth, halfLengthMinusOneBay));
          se = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -nearWidth, halfLengthMinusOneBay));

          const bottom = [sw, nw, ne, se];

          const halfLengthPlusOffset = this.feetToHorizontalPixels((this.length_ * this.bayLength) / 2 + this.parkingOffset);
          const halfLengthPlusOffseAndDepth = this.feetToHorizontalPixels((this.length_ * this.bayLength) / 2 + this.parkingOffset + this.parkingDepth);
          const halfWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth);

          sw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, halfWidth, -halfLengthPlusOffseAndDepth));
          nw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidth, -halfLengthPlusOffseAndDepth));
          ne = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidth, -halfLengthPlusOffset));
          se = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, halfWidth, -halfLengthPlusOffset));

          const left = [sw, nw, ne, se];

          sw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, halfWidth, halfLengthPlusOffseAndDepth));
          nw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidth, halfLengthPlusOffseAndDepth));
          ne = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidth, halfLengthPlusOffset));
          se = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, halfWidth, halfLengthPlusOffset));

          const right = [sw, nw, ne, se];
          return [top, bottom, left, right];
        } else {
          const halfLengthMinusOneBay = this.feetToHorizontalPixels((this.length_ * this.bayLength) / 2 - this.bayLength);
          const nearWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth);
          const farWidth = this.feetToVerticalPixels(((this.width_ - 2) * this.bayWidth) / 2 + this.speedbayWidth + this.truckCourtDepth);
          let sw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, nearWidth, -halfLengthMinusOneBay));
          let nw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, farWidth, -halfLengthMinusOneBay));
          let ne = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, farWidth, halfLengthMinusOneBay));
          let se = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, nearWidth, halfLengthMinusOneBay));

          const top = [sw, nw, ne, se];

          const halfWidthPlusOffset = this.feetToHorizontalPixels((this.width_ * this.bayWidth + this.speedbayWidth) / 2 + this.parkingOffset);
          const halfWidthPlusOffseAndDepth = this.feetToHorizontalPixels((this.width_ * this.bayWidth + this.speedbayWidth) / 2  + this.parkingOffset + this.parkingDepth);
          const halfLength = this.feetToVerticalPixels(this.length_ * this.bayLength / 2);

          sw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidthPlusOffset, -halfLength));
          nw = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidthPlusOffseAndDepth, -halfLength));
          ne = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidthPlusOffseAndDepth, halfLength));
          se = projection.fromContainerPixelToLatLng(this.computeOffset(screenCenter, -halfWidthPlusOffset, halfLength));

          const bottom = [sw, nw, ne, se];
          return [top, bottom];

        }

      }

      // Helper function to calculate the location of a new point given x and y distance and a rotation
      computeOffset(start, widthDistance, lengthDistance) {
        const x = start.x + lengthDistance * Math.cos(this.angle_) - widthDistance * Math.sin(this.angle_);
        const y = start.y + lengthDistance * Math.sin(this.angle_) + widthDistance * Math.cos(this.angle_);
        return new google.maps.Point(x, y);
      }

      // Update the center of the building when the polygon is dragged
      drag(event) {
        const newPath = this.building.getPath().getArray();
        const oldPath = this.getPath();
        this.center_ = new google.maps.LatLng(
            this.center_.lat() + newPath[0].lat() - oldPath[0].lat(),
            this.center_.lng() + newPath[0].lng() - oldPath[0].lng(),
        );

        this.updatePolygons();
      }

      // Update the rotation of the building when a corner placemarker is dragged
      rotate(event, index) {
        const projection = this.getProjection();
        const screenMarker = projection.fromLatLngToContainerPixel(event.latLng);
        const screenCorner = projection.fromLatLngToContainerPixel(this.getCorners()[index]);
        const screenCenter = projection.fromLatLngToContainerPixel(this.center_);
        const angle = Math.atan2(screenMarker.x - screenCenter.x, screenMarker.y - screenCenter.y);
        const oldAngle = Math.atan2(screenCorner.x - screenCenter.x, screenCorner.y - screenCenter.y);
        this.angle_ += oldAngle - angle;
        this.updatePolygons();
      }

      // Update the width or length of the building when an edge placemarker is dragged
      edgeScale(event, index) {
        const projection = this.getProjection();
        const oppositeEdge = this.getEdges()[(index + 2) % 4];
        const screenMarker = projection.fromLatLngToContainerPixel(event.latLng);
        const screenOppositeEdge = projection.fromLatLngToContainerPixel(oppositeEdge);

        if (index == 0 || index == 2) {
          // Update the building length
          const horizontalScreenDistance = Math.hypot(
              this.horizontalPixelsToFeet(screenMarker.x - screenOppositeEdge.x),
              this.verticalPixelsToFeet(screenMarker.y - screenOppositeEdge.y),
          );
          const newCenter = projection.fromLatLngToContainerPixel(this.center_);
          let newLength = Math.round(horizontalScreenDistance / this.bayLength);
          newLength = Math.min(Math.max(newLength, this.minLength), this.maxLength);
          let lengthDirection = 0;
          if (index == 0) {
            lengthDirection = -1;
          } else if (index == 2) {
            lengthDirection = 1;
          }
          newCenter.x += lengthDirection * this.feetToHorizontalPixels((newLength - this.length_) * this.bayLength / 2) * Math.cos(this.angle_);
          newCenter.y += lengthDirection * this.feetToHorizontalPixels((newLength - this.length_) * this.bayLength / 2) * Math.sin(this.angle_);
          this.center_ = projection.fromContainerPixelToLatLng(newCenter);
          this.length_ = newLength;
        } else if (index == 1 || index == 3) {
          // Update the building width
          const verticalScreenDistance = Math.hypot(
              this.horizontalPixelsToFeet(screenMarker.x - screenOppositeEdge.x),
              this.verticalPixelsToFeet(screenMarker.y - screenOppositeEdge.y),
          );
          const newCenter = projection.fromLatLngToContainerPixel(this.center_);
          let newWidth = Math.round(verticalScreenDistance / this.bayWidth);
          newWidth = Math.min(Math.max(newWidth, this.minWidth), this.maxWidth);
          let widthDirection = 0;
          if (index == 1) {
            widthDirection = -1;
          } else if (index == 3) {
            widthDirection = 1;
          }
          newCenter.y += widthDirection * this.feetToVerticalPixels((Math.abs(newWidth) - this.width_) * this.bayWidth / 2) * Math.cos(this.angle_);
          newCenter.x -= widthDirection * this.feetToVerticalPixels((Math.abs(newWidth) - this.width_) * this.bayWidth / 2) * Math.sin(this.angle_);
          this.center_ = projection.fromContainerPixelToLatLng(newCenter);
          this.width_ = Math.abs(newWidth);
        }
        this.updatePolygons();
      }

      updatePolygons() {
        // Update the paths for the building
        this.building.setPath(this.getPath());
        // Update the paths for the parking
        this.parkingLots_.forEach((p, i) => {
          p.setPath(this.getParkingPaths()[i]);
        });
        // Update the location of the edge placemarkers
        const edges = this.getEdges();
        this.edges_.forEach((m, i) => {
          m.setPosition(edges[i]);
          m.setZIndex(2);
          m.setCursor(this.getEdgeCursor(edges[i]));
        });
        // Update the location of the corner placemarkers
        const corners = this.getCorners();
        this.rotators_.forEach((m, i) => {
          m.setPosition(corners[i]);
          m.setZIndex(1);
          m.setCursor(this.getCornerCursor(corners[i]));
        });
      }

      // Get the correct cursor for mousing over a placemarker depending on its location relative
      // to the center
      getEdgeCursor(cursorLocation) {
        const heading = google.maps.geometry.spherical.computeHeading(cursorLocation, this.center_);
        if (heading < -157.5) {
          return 'n-resize';
        } if (heading < -112.5) {
          return 'ne-resize';
        } if (heading < -67.5) {
          return 'e-resize';
        } if (heading < -22.5) {
          return 'se-resize';
        } if (heading < 22.5) {
          return 's-resize';
        } if (heading < 67.5) {
          return 'sw-resize';
        } if (heading < 112.5) {
          return 'w-resize';
        } if (heading < 157.5) {
          return 'nw-resize';
        }
        return 'n-resize';
      }

      // Get the correct cursor for mousing over a placemarker depending on its location relative
      // to the center
      getCornerCursor(cursorLocation) {
        const heading = google.maps.geometry.spherical.computeHeading(cursorLocation, this.center_);
        if (heading < -90) {
          return 'url(\'../images/ne-rotate.png\'), auto';
        } if (heading < 0) {
          return 'url(\'../images/se-rotate.png\'), auto';
        } if (heading < 90) {
          return 'url(\'../images/sw-rotate.png\'), auto';
        }
        return 'url(\'../images/nw-rotate.png\'), auto';
      }
    }

    // Set the center and create a new overlay
    const center = new google.maps.LatLng(39.09696305509259, -94.73900612960772);
    new SitePlanOverlay(map, center, 11, 29,1, true);
    var buildingCount = 1;
  });
}

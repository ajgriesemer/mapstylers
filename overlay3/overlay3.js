var circle = {
  path: 'M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
  fillOpacity: 0.5,
  strokeWeight: 2,
  scale: 0.1
};
var bigger_circle = {
  path: 'M 0, 0 m -150, 0 a 150,150 0 2,0 300,0 a 150,150 0 2,0 -300,0',
  fillOpacity: 0.5,
  fillOpacity: 0,
  strokeWeight: 2,
  scale: 0.1
};
var pivot = {
  path: 'M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
  fillOpacity: 1,
  strokeWeight: 2,
  scale: 0.1,
  fillColor: 'white'
};


// document.addEventListener("dragenter", function(event) {
//   console.log('dragged')
//   document.getElementById("image-upload").style.borderColor = "#4d90fe"
// });

// document.addEventListener('load', function() {
//   document.querySelector('input[type="file"]').addEventListener('change', function() {
//       if (this.files && this.files[0]) {
//           var img = document.querySelector('img');  // $('img')[0]
//           img.src = URL.createObjectURL(this.files[0]); // set src to blob url
//           img.onload = imageIsLoaded;
//           console.log(img)
//       }
//   });
// });

// Dropzone.options.imageUpload = {
// }


function initMap() {
  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(39.11,-94.73),
    mapTypeId: 'hybrid'
  };
  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  var image_src = '../images/Turner Concept Plan.png';
  var image_height = 642;
  var image_width = 1001;

  class DebugOverlay extends google.maps.OverlayView {
    constructor(image, map, image_width, image_height) {
      super();
      this.image_ = image;
      this.image_width_ = image_width;
      this.image_height_ = image_height;
      this.map_ = map;
      this.div_ = null;
      this.bounds_ = null;
      this.angle_ = 0;
      this.setMap(map);
      this.hasPivot = true;
    }
    
    onAdd() {
      var overlayProjection = this.getProjection();

      var div = document.createElement('div');
      div.style.borderStyle = 'solid';
      div.style.borderWidth = '1px';
      div.style.position = 'absolute';
      var img = document.createElement('img');
      img.src = this.image_;
      img.style.width = '100%';
      img.style.height = '100%';
      img.style.position = 'absolute';
      this.div_ = div;
      this.img_ = img;
      div.appendChild(img);
      var panes = this.getPanes();
      panes.overlayLayer.appendChild(div);

      var map_bounds = this.map_.getBounds();
      var map_center = this.map_.getCenter();
      this.bounds_ = new google.maps.LatLngBounds(
        new google.maps.LatLng(
          map_center.lat() - ((map_bounds.getNorthEast().lat() - map_bounds.getSouthWest().lat()) / document.getElementById("map").offsetHeight * this.image_height_)/2,
          map_center.lng() - ((map_bounds.getNorthEast().lng() - map_bounds.getSouthWest().lng()) / document.getElementById("map").offsetWidth * this.image_width_)/2
        ),
        new google.maps.LatLng(
          map_center.lat() + ((map_bounds.getNorthEast().lat() - map_bounds.getSouthWest().lat()) / document.getElementById("map").offsetHeight * this.image_height_)/2,
          map_center.lng() + ((map_bounds.getNorthEast().lng() - map_bounds.getSouthWest().lng()) / document.getElementById("map").offsetWidth * this.image_width_)/2
        ),
      );

      this.pivot_ = new google.maps.Marker({
        position: this.getCenter(),
        map: map,
        draggable:true,
        icon: pivot
      });
      this.pivotLocation_ = this.getCenter();

      var colors = ['red','green','blue', 'yellow']
      this.rotators_ = this.getCorners().map((c,i) => {
        var colored_circle = bigger_circle;
        colored_circle.strokeColor = colors[i];
        var marker = new google.maps.Marker({
          position: c,
          map: map,
          draggable:true,
          icon: colored_circle,
          zIndex: 1
        });
        google.maps.event.addListener(marker,'drag', (e) => {this.rotate(e,i)});
        google.maps.event.addListener(marker,'dragend', (e) => {this.rotate(e,i)});
        return marker
      })
      this.corners_ = this.getCorners().map((c,i) => {
        var colored_circle = circle;
        colored_circle.strokeColor = colors[i];
        var marker = new google.maps.Marker({
          position: c,
          map: map,
          draggable:true,
          icon: colored_circle,
          zIndex: 2
        });
        google.maps.event.addListener(marker,'drag', (e) => {this.scale(e,i)});
        google.maps.event.addListener(marker,'dragend', (e) => {this.scale(e,i)});
        return marker
      })

      this.path = new google.maps.Polyline()

      this.rectangle = new google.maps.Rectangle()


      this.adjusted_pivot_ = new google.maps.Marker();
      google.maps.event.addListener(this.pivot_,'dragend', () => {this.updatePivot()});
      google.maps.event.addListener(this.pivot_,'rightclick', () => {
        this.pivot_.setMap(null);
        this.hasPivot = false;
      });
      this.map_.addListener("click", (e) => {
        if(!this.hasPivot){
          this.pivot_.setPosition(e.latLng)
          this.pivot_.setMap(this.map_);
          this.hasPivot = true;
        }
      })

    };
    draw() {
      console.log(this.angle_)
      var overlayProjection = this.getProjection();
      var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
      var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());
      var pivot = overlayProjection.fromLatLngToDivPixel(this.getPivot());

      var div = this.div_;
      this.div_.style.transformOrigin = (pivot.x - sw.x) + 'px ' + (pivot.y - ne.y) + 'px';
      this.div_.style.transform = 'rotate('+this.angle_+'deg)';
      div.style.left = sw.x + 'px';
      div.style.top = ne.y + 'px';
      div.style.width = (ne.x - sw.x) + 'px';
      div.style.height = (sw.y - ne.y) + 'px';
      
      this.rectangle.setOptions({
        bounds: this.bounds_,
        strokeColor: "#FF0000",
        strokeWeight: 2,
        map: this.map_
      })
      this.path.setOptions({
        path: [this.pivot_.getPosition(), this.getCorners()[0]],
        geodesic: true,
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 2,
        map: this.map_
      })
      
    };

    rotate(event,index) {
      console.log('rotate')
      // update angle
      var old_angle = google.maps.geometry.spherical.computeHeading(this.getCorners()[index], this.getPivot());
      var new_angle = google.maps.geometry.spherical.computeHeading(event.latLng, this.getPivot());
      this.angle_ += new_angle - old_angle

      var corners = this.getCorners();
      this.rotators_.forEach((m,i) => {
        m.setPosition(corners[i])
        m.setZIndex(1)
      })
      this.corners_.forEach((m,i) => {
        m.setPosition(corners[i])
        m.setZIndex(2)
      })
      this.draw();
    }

    scale(event,index) {
      console.log('scale')
      var old_distance = google.maps.geometry.spherical.computeDistanceBetween(this.getCorners()[index], this.pivot_.getPosition());
      var new_distance = google.maps.geometry.spherical.computeDistanceBetween(event.latLng, this.pivot_.getPosition());

      
      var sw_heading = google.maps.geometry.spherical.computeHeading(this.pivot_.getPosition(), this.getCorners()[0]);
      var sw_distance = google.maps.geometry.spherical.computeDistanceBetween(this.pivot_.getPosition(), this.getCorners()[0]);
      var ne_heading = google.maps.geometry.spherical.computeHeading(this.pivot_.getPosition(), this.getCorners()[2]);
      var ne_distance = google.maps.geometry.spherical.computeDistanceBetween(this.pivot_.getPosition(),this.getCorners()[2]);

      // find equivalent to pivot in bounds
      var bounds_pivot = google.maps.geometry.spherical.computeOffset(this.bounds_.getSouthWest(), -sw_distance, sw_heading - this.angle_)

      console.log(this.angle_, (this.pivot_.getPosition().lat() - bounds_pivot.lat()),(this.pivot_.getPosition().lng() - bounds_pivot.lng()))
      this.adjusted_pivot_.setOptions({
        position: bounds_pivot,
        map: map,
        draggable:true
      })
      this.bounds_ = new google.maps.LatLngBounds(
        //southwest
        google.maps.geometry.spherical.computeOffset(bounds_pivot,sw_distance * new_distance / old_distance,sw_heading - this.angle_), 
        //northeast
        google.maps.geometry.spherical.computeOffset(bounds_pivot,ne_distance * new_distance/ old_distance,ne_heading - this.angle_)
      );

      var corners = this.getCorners();
      this.rotators_.forEach((m,i) => {
        m.setPosition(corners[i])
      })
      this.corners_.forEach((m,i) => {
        m.setPosition(corners[i])
      })
      
      this.draw();
    }

    getCorners(){
      var sw = this.bounds_.getSouthWest()
      var nw = new google.maps.LatLng(this.bounds_.getNorthEast().lat(), this.bounds_.getSouthWest().lng()) 
      var ne = this.bounds_.getNorthEast()
      var se = new google.maps.LatLng(this.bounds_.getSouthWest().lat(), this.bounds_.getNorthEast().lng())


      var corners = [sw, nw, ne, se]
      corners = corners.map((c) => {
        var distance = google.maps.geometry.spherical.computeDistanceBetween(this.getPivot(),c);
        var heading = google.maps.geometry.spherical.computeHeading(this.getPivot(),c);
        return google.maps.geometry.spherical.computeOffset(this.getPivot(),distance,heading+this.angle_)
      });
      return corners
    }
    updatePivot(){
      console.log('update pivot')
      var lat_diff = this.pivot_.getPosition().lat() - this.getPivot().lat();
      var lng_diff = this.pivot_.getPosition().lng() - this.getPivot().lng();
      
      this.bounds_ = new google.maps.LatLngBounds(
        //southwest
        new google.maps.LatLng(
          google.maps.geometry.spherical.computeOffset(this.bounds_.getSouthWest(), - lat_diff,
          this.bounds_.getSouthWest().lng() - lng_diff), 
        //northeast
        new google.maps.LatLng(
          this.bounds_.getNorthEast().lat() - lat_diff,
          this.bounds_.getNorthEast().lng() - lng_diff)
        )
      );

      this.pivotLocation_ = this.pivot_.getPosition();
    }
      
    getPivot(){
      return this.pivotLocation_
    }
      
    getCenter(){
      return new google.maps.LatLng(
        (this.bounds_.getSouthWest().lat() + this.bounds_.getNorthEast().lat())/2,
        (this.bounds_.getSouthWest().lng() + this.bounds_.getNorthEast().lng())/2
        )
    }
    onRemove() {
      this.div_.parentNode.removeChild(this.div_);
      this.div_ = null;
    };
  }
  overlay = new DebugOverlay(image_src, map, image_width, image_height);

}

// This example requires the Drawing library. Include the libraries=drawing
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 37.94040528648475, lng: -98.38116213188299 },
    zoom: 5,
  });
  const drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.POLYGON,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        google.maps.drawing.OverlayType.POLYGON,
      ],
    },
    polygonOptions: {
      strokeColor: "#E18D3D",
      fillColor: "#E18D3D",
      fillOpacity: 0.5
    }
  });

  // Define an info window on the map.
  let infoWindow = new google.maps.InfoWindow();

  google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
    polygon.setEditable(true);
    google.maps.event.addListener(polygon.getPath(), 'insert_at', function(index, obj) {
      youMovedMe(polygon)
    });
    google.maps.event.addListener(polygon.getPath(), 'set_at', function(index, obj) {
      youMovedMe(polygon)
    });
    google.maps.event.addListener(polygon.getPath(), 'remove_at', function(index, obj) {
      youMovedMe(polygon)
    });
    drawingManager.setDrawingMode(null)
    youMovedMe(polygon)
  });
  function youMovedMe(polygon) {
    const path = polygon.getPath();
    const contentString =
      "<b>You Moved Me!</b><br>" +
      path.getArray()
  
    // Set the info window's content and position.
    infoWindow.setContent(contentString);
    infoWindow.setPosition(path.getArray()[1]);
    infoWindow.open(map);
  }
  drawingManager.setMap(map);
}
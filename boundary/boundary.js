vm1 = new Vue({
  el: "#app"
});

var map;
var origin = "";
var destinations = [];
var markers = [];
function initMap() {
  document.directionsService = new google.maps.DirectionsService();

  document.directionsDisplays = [];

  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 39.168255, lng: -94.636837 },
    zoom: 15,
    styles: style,
    mapTypeId: "satellite"
  });

  var kmlLayer = new google.maps.KmlLayer(
    "https://www.google.com/maps/d/u/0/kml?mid=1MAq1JHd5-HnOPFT2FlPDH1dlR9SFeHmt",
    {
      suppressInfoWindows: true,
      preserveViewport: false,
      map: map
    }
  );
}

document.addEventListener("keyup", function(event) {
  if (event.keyCode == "80") {
    togglePrintView();
  }
});

function togglePrintView() {
  setPrintView(!document.printView);
}

function setPrintView(value) {
  document.printView = value;
  if (vm1.$data.target) {
    vm1.$data.targetVisible = !document.printView;
  }
  if (vm1.$data.outbound1) {
    vm1.$data.outbound1Visible = !document.printView;
  }
  if (vm1.$data.outbound2) {
    vm1.$data.outbound2Visible = !document.printView;
  }
  if (vm1.$data.inbound1) {
    vm1.$data.inbound1Visible = !document.printView;
  }
  if (vm1.$data.inbound2) {
    vm1.$data.inbound2Visible = !document.printView;
  }

  let hideMarkersOptions = {
    markerOptions: {
      visible: !document.printView,
      icon: "../images/spotlight-poi2.png"
    },
    map: map
  };
  if (document.targetMarker) {
    document.targetMarker.setVisible(!document.printView);
  }
  if (document.outbound1DirectionsDisplay) {
    document.outbound1DirectionsDisplay.setOptions(hideMarkersOptions);
  }
  if (document.outbound2DirectionsDisplay) {
    document.outbound2DirectionsDisplay.setOptions(hideMarkersOptions);
  }
  if (document.inbound1DirectionsDisplay) {
    document.inbound1DirectionsDisplay.setOptions(hideMarkersOptions);
  }
  if (document.inbound2DirectionsDisplay) {
    document.inbound2DirectionsDisplay.setOptions(hideMarkersOptions);
  }

  map.setOptions({
    zoomControl: !document.printView,
    streetViewControl: !document.printView,
    mapTypeControl: !document.printView,
    fullscreenControl: !document.printView
  });
}

// Lower resolution boundaries: https://www.census.gov/geographies/mapping-files/time-series/geo/cartographic-boundary.html
// Highest resolution boundaries: https://www.census.gov/geographies/mapping-files/time-series/geo/tiger-line-file.html
// Esri Shapefile to Geojson converter https://mapshaper.org/
//    Github repo https://github.com/mbloch/mapshaper
// Sources file for Geojson to Google Maps converter https://github.com/JasonSanford/geojson-google-maps
// Definition of description fields https://www.census.gov/programs-surveys/geography/technical-documentation/records-layout/nlt-record-layouts.html

function initMap() {
  
  const mapOptions = {
    zoom: 12,
    center: new google.maps.LatLng(39.11, -94.73),
    mapTypeId: 'hybrid',
    tilt: 0,
    styles: [
      {
        'featureType': 'poi',
        'stylers': [
          {
            'visibility': 'off',
          },
        ],
      },
    ],
  };
  const map = new google.maps.Map(document.getElementById('map'), mapOptions);

  
  // Write the javascript object for Wyandotte county MO to file
  // var test = JSON.stringify(tl_2021_us_county.features.filter(c => {
  //   return c.properties.GEOID == '20209'
  // }))
  var cb_2021_wyandotte_5m_polygon = new GeoJSON(cb_2021_wyandotte_5m);
  cb_2021_wyandotte_5m_polygon.setMap(map); 
  cb_2021_wyandotte_5m_polygon.setOptions({
    strokeColor: "red"
  });
  var cb_2021_wyandotte_20m_polygon = new GeoJSON(cb_2021_wyandotte_20m);
  cb_2021_wyandotte_20m_polygon.setMap(map); 
  cb_2021_wyandotte_20m_polygon.setOptions({
    strokeColor: "blue"
  });
  var cb_2021_wyandotte_500k_polygon = new GeoJSON(cb_2021_wyandotte_500k);
  cb_2021_wyandotte_500k_polygon.setMap(map); 
  cb_2021_wyandotte_500k_polygon.setOptions({
    strokeColor: 'purple'
  });
  var tl_2021_wyandotte_polygon = new GeoJSON(tl_2021_wyandotte);
  tl_2021_wyandotte_polygon.setMap(map);
  tl_2021_wyandotte_polygon.setOptions({
    strokeColor: 'green'
  });


  var legend = document.createElement("div");
  document.body.appendChild(legend);
  legend.style = "font-family: Arial, sans-serif;background: #fff;padding: 10px;margin: 10px; border: 3px; solid #000;"

  legend.innerHTML = "<h3>Legend</h3>";
  const div1 = document.createElement("div");
  div1.innerHTML = "<p style='color:red'>cb_2021_us_county_5m";
  legend.appendChild(div1);
  const div2 = document.createElement("div");
  div2.innerHTML = "<p style='color:blue'>cb_2021_us_county_20m";
  legend.appendChild(div2);
  const div3 = document.createElement("div");
  div3.innerHTML = "<p style='color:purple'>cb_2021_us_county_500k";
  legend.appendChild(div3);
  const div4 = document.createElement("div");
  div4.innerHTML = "<p style='color:green'>tl_2021_us_county";
  legend.appendChild(div4);
  

  map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
  // tl_2021_29_place.features.forEach(county => {
  //   if(county.geometry.type == 'MultiPolygon'){
  //     var myGoogleVector = new GeoJSON(county);
      
  //     myGoogleVector.forEach(polygon => polygon.setMap(map))   
  //   }
  //   else{
  //     var polygon = new GeoJSON(county);
      
  //     google.maps.event.addListener(polygon, 'click', (e) => {
  //       polygon.setOptions({
  //         strokeColor: '#afd'
  //       });
  //       const infowindow = new google.maps.InfoWindow({
  //         content: JSON.stringify(county.properties),
  //         position:e.latLng
  //       });
  //       infowindow.open({
  //         map,
  //         shouldFocus: false,
  //       });
  //     });
      
  //     polygon.setMap(map);   
  //   } 
  // })
}

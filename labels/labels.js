let map;
document.drawingEnabled = false;
document.isDrawing = false; 

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 39.141515920577504, lng: -94.6092618464566 },
    zoom: 15,
    styles: style,
    mapTypeId: "hybrid",
    gestureHandling: "greedy" //For mobile, this allows the map to drag on a swipe instead of scroll
  });

  // Create the DIV to hold the control and call the CenterControl()
  // constructor passing in this DIV.
  const controlDiv = document.createElement("div");

  const controlUI = document.createElement("div");
  controlUI.className = "centerControl"
  controlUI.title = "Click to recenter the map";
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  const controlText = document.createElement("div");
  controlText.className = "controlText"
  controlText.innerHTML = "Start Drawing";
  controlUI.appendChild(controlText);
  // Setup the click event listeners: simply set the map to Chicago.
  controlUI.addEventListener("click", () => {
    addLabel()
  });
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(controlDiv);
  function addLabel(){
    var label = new LabelOverlay();
  }
  class LabelOverlay extends google.maps.OverlayView {
    constructor() {
        super();
        this.topLeft_ = map.getCenter();
        this.setMap(map);
        this.map_ = map;
        this.selected = false;
        this.baseFontSize = 48;
        this.baseStrokeWidth = 1;
    }
    onAdd() {
      const panes = this.getPanes();
      const projection = this.getProjection()
      this.div_ = document.createElement('div');
      this.div_.id = "custom-label"
      //this.div_.style.fontWeight = 800; //fontWeight is doing a weird thing where there is a split second where it doesn't apply on the initial load
      this.div_.style.borderWidth = '0px';
      this.div_.style.position = 'absolute';
      this.div_.style.fontFamily = 'Roboto,Arial,sans-serif';
      this.div_.style.fontSize = this.baseFontSize + 'px';
      this.div_.style.webkitTextStroke = '1px black';
      this.div_.style.color = 'white';
      this.div_.style.left = projection.fromLatLngToDivPixel(this.topLeft_).x + 'px';
      this.div_.style.top = projection.fromLatLngToDivPixel(this.topLeft_).y + 'px';
      
      this.div_.innerText = "FAIRFAX"
      panes.overlayLayer.appendChild(this.div_);

      this.clickArea_ = new google.maps.Polygon({
        strokeOpacity: 1,
        strokeWeight: 0,
        fillColor: '#FF0000',
        fillOpacity: 0,
        map: map,
        paths: [this.getCorners()],
        draggable: false,
        });
        
      this.corners_ = this.getCorners().map((c, i) => {
        const marker = new google.maps.Marker({
            position: c,
            draggable: true,
            icon: square,
            zIndex: 2,
        });
        google.maps.event.addListener(marker, 'drag', (e) => {
            this.scale(e, i);
        });
        google.maps.event.addListener(marker, 'dragend', (e) => {
            this.scale(e, i);
        });
        return marker;
      });
      google.maps.event.addListener(this.clickArea_, 'drag', (e) => {
        this.drag(e);
      });
      google.maps.event.addListener(this.clickArea_, 'click', (e) => {
        if(!this.selected){
          this.selected = true;
          this.clickArea_.setOptions(
            {draggable: true,
            strokeWeight: 1}
          )
          this.corners_.forEach((m) => {
              m.setMap(this.map_);
          });
        } else {
          this.selected = false;
          this.clickArea_.setOptions(
            {draggable: false,
            strokeWeight: 0}
          )
          this.corners_.forEach((m) => {
              m.setMap(null);
          });
        }
      });

      map.addListener("click", () => {
        if(this.selected){
          this.selected = false;
          this.clickArea_.setOptions(
            {draggable: false,
            strokeWeight: 0}
          )
          this.corners_.forEach((m) => {
              m.setMap(null);
          });
        }
      });
      // google.maps.event.addListener(this.clickArea_, 'dblclick', (e) => {
      //   this.input_ = document.createElement("input");
      //   this.input_.setAttribute('type', 'text');
      //   this.input_.value = "Riverside Horizons"
      //   this.input_.focus();
      //   this.input_.select();
      //   this.div_.appendChild(this.input_)
      // });
      const worldWidth = projection.getWorldWidth();
      this.originalWorldWidth = worldWidth;

    };
    draw() {
      const projection = this.getProjection();
      const worldWidth = projection.getWorldWidth();
      const corners = this.getCorners();
      this.div_.style.fontSize = this.baseFontSize / this.originalWorldWidth * worldWidth + 'px';
      this.div_.style.webkitTextStroke = this.baseStrokeWidth / this.originalWorldWidth * worldWidth + 'px black';
      this.div_.style.left = projection.fromLatLngToDivPixel(this.topLeft_).x + 'px';
      this.div_.style.top = projection.fromLatLngToDivPixel(this.topLeft_).y + 'px';
      this.clickArea_.setPath(corners);        
      this.corners_.forEach((m, i) => {
        m.setPosition(corners[i]);
        m.setZIndex(1);
      });
    }
    getCorners(){
      const projection = this.getProjection();
      const topLeftInPixels = projection.fromLatLngToContainerPixel(this.topLeft_);

      return [
        projection.fromContainerPixelToLatLng(new google.maps.Point(topLeftInPixels.x, topLeftInPixels.y + this.div_.offsetHeight)), //sw
        projection.fromContainerPixelToLatLng(new google.maps.Point(topLeftInPixels.x, topLeftInPixels.y)), //nw
        projection.fromContainerPixelToLatLng(new google.maps.Point(topLeftInPixels.x + this.div_.offsetWidth, topLeftInPixels.y)), //ne
        projection.fromContainerPixelToLatLng(new google.maps.Point(topLeftInPixels.x + this.div_.offsetWidth, topLeftInPixels.y + this.div_.offsetHeight)), //se
      ];
    }
    drag(e){
      const newPath = this.clickArea_.getPath().getArray();
      const oldCorners = this.getCorners();
      this.topLeft_ = new google.maps.LatLng(
          this.topLeft_.lat() + newPath[0].lat() - oldCorners[0].lat(),
          this.topLeft_.lng() + newPath[0].lng() - oldCorners[0].lng(),
      );

    }
    scale(event, index){
      const projection = this.getProjection();
      const corners = this.getCorners();
      const screenOppositeCorner = projection.fromLatLngToContainerPixel(corners[(index+2)%4]);
      const screenOldCorner = projection.fromLatLngToContainerPixel(corners[index]);
      const screenEventMarker = projection.fromLatLngToContainerPixel(event.latLng);
      const oppositeToOld = new Vector(screenOldCorner.x - screenOppositeCorner.x, screenOldCorner.y - screenOppositeCorner.y);
      const oppositeToEvent = new Vector(screenEventMarker.x - screenOppositeCorner.x, screenEventMarker.y - screenOppositeCorner.y);
      const scaleFactor = oppositeToEvent.length() / oppositeToOld.length();
      this.baseStrokeWidth = scaleFactor * this.baseStrokeWidth;
      this.baseFontSize = scaleFactor * this.baseFontSize;
      this.draw();

    }
  }
}

const square = {
  path: 'M  -5, -5 L -5,5 L 5,5 L 5,-5 L -5,-5',
  fillOpacity: 1,
  fillColor: 'white',
  strokeWeight: 1,
  scale: 1,
};

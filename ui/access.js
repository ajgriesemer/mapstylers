vm1 = new Vue({
  el: "#app",
  data: function () {
    return {
      destinations: [],
      results: [],
      targetSearch: null,
      target: null,
    };
  },
});

var map;
var start = "";
var end = "";

function initMap() {
  document.overlay = new google.maps.OverlayView();
  document.directionsService = new google.maps.DirectionsService();
  document.directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: true,
    polylineOptions: {
      strokeColor: "red",
    },
  });
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 39.167747, lng: -94.636855 },
    zoom: 15,
    styles: style,
    mapTypeId: "hybrid",
    mapTypeControl: false, //Hide map type controls
  });

  document.sessionToken = new google.maps.places.AutocompleteSessionToken();
  document.autocompleteService = new google.maps.places.AutocompleteService();
  document.placesService = new google.maps.places.PlacesService(map);

  document.directionsDisplay.setMap(map);
}
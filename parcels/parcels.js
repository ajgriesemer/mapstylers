  function parseColor(input) {
      var div = document.createElement('div');
      div.style.display = 'none';
      div.style.color = input;
      document.body.appendChild(div);
      var m = window.getComputedStyle(div).color.match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i);
      document.body.removeChild(div);
      if (m) {
        return [m[1],m[2],m[3]];
      } else {
        alert("REP Overlays error: color could not be parsed: " + input);
        return null;
      }
  }
  
  var REP = ({});
  REP.ClientKey = 'FVv7W5Az99';
  REP.ClientKeyOrTokenParam = 'client=' + REP.ClientKey;
  REP.Layer = ({});
  REP.Layer.Parcels_OutlineColor = null;
  REP.Layer.Return_Buildings = false;
  REP.Layer.Parcels_IdentifyVersion = '3';
  
  REP.Layer.Google = ({});
  REP.Layer.Google.MIN_ZOOM = 14;
  REP.Layer.Google.TmpTilesRequested = {};
  
  REP.Layer.Google.InfoWindow = null;
  REP.Layer.Google.MapFeatures = [];
  
  REP.Layer.Google.parcelsURIConstructor = function(coord, zoom) {
    if (zoom >= REP.Layer.Google.MIN_ZOOM) {
      if (typeof(REP.Layer.Google.TmpTilesRequested[JSON.stringify([coord,zoom])]) === 'undefined') {
        REP.Layer.Google.TmpTilesRequested[JSON.stringify([coord,zoom])] = true;
      }
      //var tileServer = REP.Layer.selectParcelsTileServer(coord.x, coord.y, zoom);
      var url = "https://reportallusa.com/dyn/tile.py?map=siteroot/Base_Layers.map&layer=Parcels&mode=tile&tilemode=gmap&tile=" + coord.x + '+' + coord.y + '+' + zoom + '&' + REP.ClientKeyOrTokenParam;
      if (REP.Layer.Parcels_OutlineColor) {
        url += "&palette_recolor=" + REP.Layer.Parcels_OutlineColor.join(" ");
      }
      return url;
    }
    return null;
  };
  
  REP.Layer.Google._WktToMapFeatures = function(wkt_str, map) {
    var mapFeatures = [];
    if (wkt_str !== null) {
      var wktReader = new Wkt.Wkt();
      wktReader.read(wkt_str);  // Could throw exception.
      var featureGeom = wktReader.toObject(map.defaults);
      if (Wkt.isArray(featureGeom)) {
        for (var j in featureGeom) {
          if (featureGeom.hasOwnProperty(j) && !Wkt.isArray(featureGeom[j])) {
            mapFeatures.push(featureGeom[j]);
          }
        }
      } else {
        mapFeatures.push(featureGeom);
      }
    }
    return mapFeatures;
  }
  
  REP.Layer.Google.IdentifyByPoint = function(map, latLng, successCallback, failureCallback) {
    REP.Layer.IdentifyByPoint(map, latLng.lng(), latLng.lat(), function (respJson) {
      for (var i = 0; i < respJson.results.length; i++) {
        var respRowI = respJson.results[i];
        var mapFeatures = [];
        var buildingsPolyFeatures = [];
        if (typeof(respRowI['geom_as_wkt']) !== 'undefined') {
          var respRowIGeom = respRowI['geom_as_wkt'];
          if (respRowIGeom !== null) {
            /*
            var wktReader = new Wkt.Wkt();
            try {
              wktReader.read(respRowIGeom);
            } catch (err) {
              if (err.name === 'WKTError') {
                if (typeof(console) !== 'undefined' && typeof(console.log) !== 'undefined') {
                  console.log('REP Overlays error: Could not parse WKT of record ' + i);
                }
              }
            }
            var featureGeom = wktReader.toObject(map.defaults);
            if (Wkt.isArray(featureGeom)) {
              for (var j in featureGeom) {
                if (featureGeom.hasOwnProperty(j) && !Wkt.isArray(featureGeom[j])) {
                  mapFeatures.push(featureGeom[j]);
                }
              }
            } else {
              mapFeatures.push(featureGeom);
            }
            */
            try {
              mapFeatures = REP.Layer.Google._WktToMapFeatures(respRowIGeom, map);
            } catch (err) {
              if (err.name === 'WKTError') {
                if (typeof(console) !== 'undefined' && typeof(console.log) !== 'undefined') {
                  console.log('REP Overlays error: Could not parse WKT of record ' + i);
                }
              }
            }
          }
          delete respRowI['geom_as_wkt'];
        }
        respRowI['geom'] = mapFeatures;
  
        if (typeof(respRowI['buildings_poly']) !== 'undefined') {
          var buildings_poly_arr = respRowI['buildings_poly'];
          if (buildings_poly_arr !== null) {
            for (j = 0; j < buildings_poly_arr.length; j++) {
              var bp_rec_j = buildings_poly_arr[j];
              var bldgFeatures = [];
              if (typeof(bp_rec_j['geom_as_wkt']) !== 'undefined') {
                var bp_geom_wkt = bp_rec_j['geom_as_wkt'];
                if (bp_geom_wkt !== null) {
                  try {
                    bldgFeatures = REP.Layer.Google._WktToMapFeatures(bp_geom_wkt, map);
                  } catch (err) {
                    if (err.name === 'WKTError') {
                      if (typeof(console) !== 'undefined' && typeof(console.log) !== 'undefined') {
                        console.log('REP Overlays error: Could not parse building record ' + j + ' WKT of parcel record ' + i);
                      }
                    }
                  }
                }
                delete bp_rec_j['geom_as_wkt'];
              }
              bp_rec_j['geom'] = bldgFeatures;
            }
          }
        }
      }
      successCallback(respJson);
    }, failureCallback);
  }
  
  
  REP.Layer.Google.Initialize = function(map, options) {
    REP.Layer.Initialize(map, options);
    var parcelLayerOptions = {
      name: "Parcels",
      getTileUrl: REP.Layer.Google.parcelsURIConstructor,
      tileSize: new google.maps.Size(256,256)
    };
    map.overlayMapTypes.insertAt(0, new google.maps.ImageMapType(parcelLayerOptions));
  
  }
  
  
  // Return number of tiles requested since page was loaded.
  REP.Layer.Google.ParcelTilesRequestedCount = function() {
    var num_tile_req = 0;
    for (var tile_req in REP.Layer.Google.TmpTilesRequested) {
      num_tile_req++;
    }
    return num_tile_req;
  }
  
  
  REP.Layer.Initialize = function(map, options) {
    if (options) {
      if (typeof(options.Parcels_OutlineColor) !== "undefined") {
        var parcels_outlinecolor = parseColor(options.Parcels_OutlineColor);
        if (parcels_outlinecolor !== null) {
          REP.Layer.Parcels_OutlineColor = parcels_outlinecolor;
        }
      }
      if (typeof(options.Return_Buildings) !== "undefined") {
        if (options.Return_Buildings) {
          REP.Layer.Return_Buildings = true;
        }
      }
    }
  }
  
  REP.Layer.selectParcelsTileServer = function(x, y, z) {
    return REP.Layer.selectTileServer([x, y, z, 'siteroot/Base_Layers.map', 'Parcels'].join('&'));
  }
  
  // Selects a tile server by hashing paramString, in the same way as OpenLayers.
  REP.Layer.selectTileServer = function(s) {
    var NUM_TILE_SERVERS = 4;
    var GOLDEN_RATIO = (Math.sqrt(5) - 1) / 2;
    var product = 1;
    for (var i=0, len=s.length; i<len; i++) {
      product *= s.charCodeAt(i) * GOLDEN_RATIO;
      product -= Math.floor(product);
    }
    return Math.floor(product * NUM_TILE_SERVERS);
  }
  
  REP.Layer.IdentifyByPoint = function(map, longitude, latitude, successCallback, failureCallback) {
    var detUrl = "https://reportallusa.com/api/parcels.php?" + REP.ClientKeyOrTokenParam + "&v=" + REP.Layer.Parcels_IdentifyVersion + "&spatial_intersect=POINT(" + longitude + "%20" + latitude + ")&si_srid=4326";
    if (REP.Layer.Return_Buildings) {
      detUrl += "&return_buildings=t";
    }
    var xhr = new XMLHttpRequest();
    if (failureCallback) {
      xhr.ontimeout = function() {
        failureCallback({message: 'IdentifyByPoint request timed out'});
      }
    }
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {  // complete
        if (xhr.status === 200) {
          var respText = xhr.responseText;
          var respJson = JSON.parse(respText);  // TODO: Handle parsing error.
          if (typeof(respJson.status) !== 'undefined' && respJson.status === 'error') {
            if (failureCallback) {
              //alert("REP Overlays error: Detail response returned message: " + respJson.message);
              failureCallback(respJson);
            }
            return;
          }
          successCallback(respJson);
        } else {
          if (failureCallback) {
            failureCallback({message: 'IdentifyByPoint request returned response code ' + xhr.status, code: xhr.status});
          }
        }
      }
    }
    xhr.open('GET', detUrl);
    xhr.timeout = 60000;
    xhr.send();
  
  }
  
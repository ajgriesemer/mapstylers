const circle = {
  path: 'M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
  fillOpacity: 0.5,
  strokeWeight: 2,
  scale: 0.1,
};
const biggerCircle = {
  path: 'M 0, 0 m -150, 0 a 150,150 0 2,0 300,0 a 150,150 0 2,0 -300,0',
  fillOpacity: 0.5,
  fillOpacity: 0,
  strokeWeight: 2,
  scale: 0.1,
};
const pivot = {
  path: 'M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
  fillOpacity: 1,
  strokeWeight: 2,
  scale: 0.1,
  fillColor: 'white',
};
const square = {
  path: 'M  -5, -5 L -5,5 L 5,5 L 5,-5 L -5,-5',
  fillOpacity: 1,
  fillColor: 'white',
  strokeWeight: 1,
  scale: 1,
};
document.overlays = []
document.parcelPolygons = []
document.imageIsEditable = false; 
document.applyClipping = false; 
document.editableParcels = true;

document.onkeydown = function(e) {
  if (e.key == 'Shift') {
    document.shiftPressed = true;
  }
};
document.onkeyup = function(e) {
  if (e.key == 'Shift') {
    document.shiftPressed = false;
  }
  if (e.key == 'a') {
    if (document.overlays.length > 0) {
      document.imageIsEditable = !document.imageIsEditable; 
      document.overlays.forEach(o => {
        o.updateImageEdit()
      });
    }
  }
  if (e.key == 's') {
    if (document.overlays.length > 0) {
      document.applyClipping = !document.applyClipping; 
      document.overlays.forEach(o => {
        o.updateClipping();
      });
    }
  }
  if (e.key == 'd') {
    if (document.overlays.length > 0) {
      document.editableParcels = !document.editableParcels;
      document.overlays.forEach(o => {
        o.updateEditableParcels();
      });
    }
  }
};

function createDownloadButton(map) {
  if(document.overlays.length == 0){
    document.centerControlDiv = document.createElement('div');

    // Set CSS for the control border.
    document.controlUI = document.createElement('div');
    document.controlUI.style.backgroundColor = '#fff';
    document.controlUI.style.border = '2px solid #fff';
    document.controlUI.style.borderRadius = '3px';
    document.controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    document.controlUI.style.cursor = 'pointer';
    document.controlUI.style.marginBottom = '22px';
    document.controlUI.style.marginTop = '10px';
    document.controlUI.style.textAlign = 'centerasa';
    document.centerControlDiv.appendChild(document.controlUI);

    // Set CSS for the control interior.
    document.controlText = document.createElement('div');
    document.controlText.style.color = 'rgb(25,25,25)';
    document.controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    document.controlText.style.fontSize = '16px';
    document.controlText.style.lineHeight = '38px';
    document.controlText.style.paddingLeft = '5px';
    document.controlText.style.paddingRight = '5px';
    document.controlText.innerHTML = 'Download KMZ';
    document.controlUI.appendChild(document.controlText);

    document.controlUI.addEventListener('click', () => {
      if (document.overlays.length > 0) {
        document.overlays.forEach(o => {
          o.downloadKMZ();
        }); 
      }
    });
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.centerControlDiv);
  }
}
function initMap() {
  const dropArea = document.getElementById('dropzone');

  ['dragenter', 'dragover', 'dragleave', 'drop'].forEach((eventName) => {
    dropArea.addEventListener(eventName, preventDefaults, false);
  });

  function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
  }
  ['dragenter', 'dragover'].forEach((eventName) => {
    document.addEventListener(eventName, highlight, false);
  });
  ['drop'].forEach((eventName) => {
    document.addEventListener(eventName, loadDropped, false);
    dropArea.addEventListener(eventName, loadDropped, false);
  });

  function highlight(e) {
    dropArea.classList.add('highlight');
  }

  function loadDropped(e) {
    dropArea.classList.remove('highlight');
    if (!document.imageLoaded) {
      const reader = new FileReader();
      const imageName = e.dataTransfer.files[0].name;
      reader.readAsDataURL(e.dataTransfer.files[0]);
      reader.onloadend = function() {
        initImage(map, reader.result, imageName, new google.maps.Point(e.clientX, e.clientY));
      };
    }
  }
  const mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(39.11, -94.73),
    mapTypeId: 'hybrid',
    tilt: 0,
    mapId: "2ac1dcb7880cb5ca",
    disableDoubleClickZoom: true,
    isFractionalZoomEnabled: true
  };
  const map = new google.maps.Map(document.getElementById('map'), mapOptions);

  let mapFeatures = [];
  REP.Layer.Google.Initialize(map);
  // Cause click on map to show attribute popup and highlight parcel.
  google.maps.event.addListener(map, 'click', function(event) {
    if (map.getZoom() < REP.Layer.Google.MIN_ZOOM) return;

    mapFeatures = [];

    const latLng = event.latLng;

    REP.Layer.Google.IdentifyByPoint(map, latLng, function(resp) {
      const wText = '';
      if (resp.results.length) {
        const respRow0 = resp.results[0];
        for (const respKey in respRow0) {
          const respVal = respRow0[respKey];
          if (respVal === null) continue;
          if (respKey === 'geom') {
            // Add parcel geometry (possibly multiple if multipart) to map.
            if (document.overlays.length > 0) {
              for (let i = 0; i < respVal.length; i++) {
                document.overlays[0].addParcelOutline(respVal[i]);
              }
            }
          }
        }
      }
    }, function(errObj) {
      alert('REP Overlays error: ' + errObj.message);
    });
  });

  function initImage(map, imageSrc, imageName, dropLocation) {
    const image = new Image();
    image.src = imageSrc;

    image.onload = function() {
      const imageWidth = this.width;
      const imageHeight = this.height;
      createDownloadButton(map)

      document.overlays.push(new ImageOverlay(imageSrc, map, imageWidth, imageHeight, dropLocation, imageName))
    };
  }
  
  class ImageOverlay extends google.maps.OverlayView {
    constructor(image, map, imageWidth, imageHeight, dropLocation, imageName) {
        super();
        this.image_ = image;
        this.map_ = map;
        this.div_ = null;
        this.angle_ = 0;
        this.dropLocation_ = dropLocation;
        this.hasPivot = false;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.imageName = imageName;
        this.svgPaths = [];
        this.setMap(map);
    }

    onAdd() {
        //Initialize the div and image
        const div = document.createElement('div');
        div.style.borderStyle = 'solid';
        div.style.borderWidth = '1px';
        div.style.position = 'absolute';
        this.img_ = document.createElement('img');
        this.img_.src = this.image_;
        this.img_.style.width = '100%';
        this.img_.style.height = '100%';
        this.img_.style.position = 'absolute';
        this.div_ = div;
        div.appendChild(this.img_);
        const panes = this.getPanes();
        this.imageOverlayIndex = document.overlays.length

        this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this.svgDefs = this.svg.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'defs'));
        this.svgClipPath = this.svgDefs.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'clipPath'));
        this.svgClipPath.setAttribute('id', 'svgClip' + this.imageOverlayIndex);
        div.appendChild(this.svg);
        panes.overlayLayer.appendChild(div);

        const halfScreenHeight = document.getElementById('map').offsetHeight/2;
        const adjustedImageWidth = halfScreenHeight / this.imageHeight * this.imageWidth;
        const projection = this.getProjection();
        const worldWidth = projection.getWorldWidth();
        
        this.center_ = projection.fromContainerPixelToLatLng(this.dropLocation_);

        this.imageRatioToWorld = new google.maps.Point(adjustedImageWidth/worldWidth, halfScreenHeight/worldWidth);

        const colors = ['red', 'green', 'blue', 'yellow'];
        this.rotators_ = this.getCorners().map((c, i) => {
        const invisibleCircle = biggerCircle;
        invisibleCircle.strokeColor = '#FF00ff00';
        const marker = new google.maps.Marker({
            position: c,
            map: map,
            draggable: true,
            icon: invisibleCircle,
            zIndex: 1,
            cursor: this.getRotatorCursor(c),
        });
        google.maps.event.addListener(marker, 'drag', (e) => {
            this.rotate(e, i)
            ;
        });
        google.maps.event.addListener(marker, 'dragend', (e) => {
            this.rotate(e, i)
            ;
        });
        return marker;
        });
        const scaleDirections = [
        {x: 1, y: -1},
        {x: 1, y: 1},
        {x: -1, y: 1},
        {x: -1, y: -1},
        ];
        this.corners_ = this.getCorners().map((c, i) => {
          const marker = new google.maps.Marker({
              position: c,
              map: map,
              draggable: true,
              icon: square,
              zIndex: 2,
          });
          marker.scaleDirection = scaleDirections[i];
          google.maps.event.addListener(marker, 'drag', (e) => {
              this.scale(e, i)
              ;
          });
          google.maps.event.addListener(marker, 'dragend', (e) => {
              this.scale(e, i)
              ;
          });
          return marker;
        });

        const edgeDirection = ['y', 'x', 'y', 'x'];
        const edgeSign = [1, 1, -1, -1];
        this.edges_ = this.getEdges().map((c, i) => {
        const marker = new google.maps.Marker({
            position: c,
            map: map,
            draggable: true,
            icon: square,
            zIndex: 2,
            cursor: this.getScaleCursor(c),
        });
        marker.direction = edgeDirection[i];
        marker.sign = edgeSign[i];
        google.maps.event.addListener(marker, 'drag', (e) => {
            this.edgeScale(e, i, marker)
            ;
        });
        google.maps.event.addListener(marker, 'dragend', (e) => {
            this.edgeScale(e, i, marker)
            ;
        });
        return marker;
        });

        this.pivot_ = new google.maps.Marker({
        position: this.center_,
        draggable: true,
        icon: pivot,
        });

        this.clickArea_ = new google.maps.Polygon({
        strokeOpacity: 0,
        strokeWeight: 0,
        fillColor: '#FF0000',
        fillOpacity: 0,
        map: this.map_,
        paths: [this.getCorners()],
        draggable: true,
        });
        google.maps.event.addListener(this.clickArea_, 'dblclick', (e) => {
          if (!this.hasPivot) {
              this.pivot_.setPosition(e.latLng);
              this.pivot_.setMap(this.map_);
              this.hasPivot = true;
          }
        });
        google.maps.event.addListener(this.clickArea_, 'drag', (e) => {
        this.drag(e);
        });

        google.maps.event.addListener(this.pivot_, 'rightclick', () => {
        this.pivot_.setMap(null);
        this.hasPivot = false;
        });
        this.accumulated_error_lat = 0;
        this.accumulated_error_lng = 0;
        this.offset_lat_ = 0;
        this.offset_lng_ = 0;
    };

    draw() {
        const projection = this.getProjection();

        const centerInPixels = projection.fromLatLngToDivPixel(this.center_);
        const imageSize = this.getImageSizeInPixels();

        const div = this.div_;
        this.div_.style.transform = 'rotate('+this.angle_+'rad)';
        div.style.left = centerInPixels.x - imageSize.x/2 + 'px';
        div.style.top = centerInPixels.y - imageSize.y/2 + 'px';
        div.style.width = imageSize.x + 'px';
        div.style.height = imageSize.y + 'px';

        while (this.svgClipPath.firstChild) {
          this.svgClipPath.removeChild(this.svgClipPath.firstChild);
        }
        document.parcelPolygons.forEach((parcel, index) => {
          const path = parcel.getPath().getArray();
          let pathString = '';
          pathString = '';
          path.forEach((p, i) => {
              const point = projection.fromLatLngToDivPixel(p);
              const pathPoint = new google.maps.Point(
                  point.x - (centerInPixels.x - imageSize.x/2),
                  point.y - (centerInPixels.y - imageSize.y/2));
              const rotatedPathPoint = new google.maps.Point(
                  imageSize.x/2 + (pathPoint.x - imageSize.x/2) * Math.cos(-this.angle_) - (pathPoint.y - imageSize.y/2) * Math.sin(-this.angle_),
                  imageSize.y/2 + (pathPoint.x - imageSize.x/2) * Math.sin(-this.angle_) + (pathPoint.y - imageSize.y/2) * Math.cos(-this.angle_));
              pathString += `${i == 0 ? 'M' : 'L'} ${rotatedPathPoint.x}, ${rotatedPathPoint.y} `;
          });
          pathString += 'Z';
          const children = this.svgClipPath.childNodes;
          this.svgPaths[index] = document.createElementNS('http://www.w3.org/2000/svg', 'path');
          this.svgPaths[index].setAttribute('d', pathString);
          this.svgClipPath.appendChild(this.svgPaths[index]);
        });
    };

    rotate(event, index) {
        let pivot;
        if (this.hasPivot) {
        pivot = this.pivot_.getPosition();
        } else {
        pivot = this.center_;
        }
        const projection = this.getProjection();
        const eventMarker = projection.fromLatLngToContainerPixel(event.latLng);
        const screenCenter = projection.fromLatLngToContainerPixel(this.center_);
        let screenPivot;
        if (this.hasPivot) {
        screenPivot = projection.fromLatLngToContainerPixel(pivot);
        } else {
        screenPivot = screenCenter;
        }

        const oldMarker = this.getCornersInPixels()[index];
        // update angle
        const oldAngle = Math.atan2(-(oldMarker.y - screenPivot.y), oldMarker.x - screenPivot.x);
        const newAngle = Math.atan2(-(eventMarker.y - screenPivot.y), eventMarker.x - screenPivot.x);
        const deltaAngle = oldAngle - newAngle;
        this.angle_ += deltaAngle;
        const test = this.angle_;
        if (this.angle_ > Math.PI) {
        this.angle_ -= 2 * Math.PI;
        }
        if (this.angle_ < -Math.PI) {
        this.angle_ += 2 * Math.PI;
        }

        if (this.hasPivot) {
        const newScreenCenter = new google.maps.Point(
            screenPivot.x + (screenCenter.x - screenPivot.x) * Math.cos(deltaAngle) - (screenCenter.y - screenPivot.y) * Math.sin(deltaAngle),
            screenPivot.y + (screenCenter.x - screenPivot.x) * Math.sin(deltaAngle) + (screenCenter.y - screenPivot.y) * Math.cos(deltaAngle));
        this.center_ = projection.fromContainerPixelToLatLng(newScreenCenter);
        }
        this.updatePlacemarkers();
    }

    scale(event, index) {
        const projection = this.getProjection();
        const corners = this.getCorners();
        const screenOldCorner = projection.fromLatLngToContainerPixel(corners[index]);
        const screenEventMarker = projection.fromLatLngToContainerPixel(event.latLng);
        if (this.hasPivot) {
        const screenPivot = projection.fromLatLngToContainerPixel(this.pivot_.getPosition());
        const pivotToEvent = new Vector(screenEventMarker.x - screenPivot.x, screenEventMarker.y - screenPivot.y);
        const pivotToOldCorner = new Vector(screenOldCorner.x - screenPivot.x, screenOldCorner.y - screenPivot.y);
        if (document.shiftPressed) {
            const scaleFactor = pivotToEvent.length() / pivotToOldCorner.length();
            this.scaleFromFactors(scaleFactor, scaleFactor, this.pivot_.getPosition());
        } else {
            const scaleFactorX = pivotToEvent.projectOnAngle(this.angle_).length() / pivotToOldCorner.projectOnAngle(this.angle_).length();
            const scaleFactorY = pivotToEvent.projectOnAngle(this.angle_ - Math.PI/2).length() / pivotToOldCorner.projectOnAngle(this.angle_ - Math.PI/2).length();

            this.scaleFromFactors(scaleFactorX, scaleFactorY, this.pivot_.getPosition());
        }
        } else {
        const screenOppositeCorner = projection.fromLatLngToContainerPixel(corners[(index+2)%4]);
        const oppositeToOld = new Vector(screenOldCorner.x - screenOppositeCorner.x, screenOldCorner.y - screenOppositeCorner.y);
        const oppositeToEvent = new Vector(screenEventMarker.x - screenOppositeCorner.x, screenEventMarker.y - screenOppositeCorner.y);

        if (document.shiftPressed) {
            const scaleFactor = oppositeToEvent.length() / oppositeToOld.length();
            this.scaleFromFactors(scaleFactor, scaleFactor, corners[(index+2)%4]);
        } else {
            const scaleFactorX = oppositeToEvent.projectOnAngle(this.angle_).length() / oppositeToOld.projectOnAngle(this.angle_).length();
            const scaleFactorY = oppositeToEvent.projectOnAngle(this.angle_ - Math.PI/2).length() / oppositeToOld.projectOnAngle(this.angle_ - Math.PI/2).length();

            this.scaleFromFactors(scaleFactorX, scaleFactorY, corners[(index+2)%4]);
        }
        }
        this.updatePlacemarkers();
    }

    scaleFromFactors(scaleFactorX, scaleFactorY, pivot) {
        const projection = this.getProjection();
        const screenCenter = projection.fromLatLngToContainerPixel(this.center_);
        const screenPivot = projection.fromLatLngToContainerPixel(pivot);
        const pivotToCenter = new Vector(screenCenter.x - screenPivot.x, screenCenter.y - screenPivot.y);

        this.imageRatioToWorld.x = this.imageRatioToWorld.x * scaleFactorX;
        this.imageRatioToWorld.y = this.imageRatioToWorld.y * scaleFactorY;

        const pivotToCenterOnWidth = pivotToCenter.projectOnAngle(this.angle_).length() * scaleFactorX * Math.sign(pivotToCenter.projectOnAngle(this.angle_).components[0]);
        const pivotToCenterOnHeight = pivotToCenter.projectOnAngle(this.angle_ + Math.PI/2).length() * scaleFactorY * Math.sign(pivotToCenter.projectOnAngle(this.angle_+ Math.PI/2).components[1]);
        const newScreenCenter = new google.maps.Point(
            screenPivot.x + Math.sign(Math.cos(this.angle_)) * (pivotToCenterOnWidth * Math.cos(this.angle_) - pivotToCenterOnHeight * Math.sin(this.angle_)),
            screenPivot.y + Math.sign(Math.cos(this.angle_)) * (pivotToCenterOnWidth * Math.sin(this.angle_) + pivotToCenterOnHeight * Math.cos(this.angle_)),
        );
        this.center_ = projection.fromContainerPixelToLatLng(newScreenCenter);
    }

    edgeScale(event, index, marker) {
        const edges = this.getEdges();
        const projection = this.getProjection();
        const screenOldEdge = projection.fromLatLngToContainerPixel(edges[index]);
        const screenEventMarker = projection.fromLatLngToContainerPixel(event.latLng);
        const screenPivot = projection.fromLatLngToContainerPixel(this.pivot_.getPosition());
        const screenCenter = projection.fromLatLngToContainerPixel(this.center_);
        if (this.hasPivot) {
        const pivotToOld = new Vector(screenOldEdge.x - screenPivot.x, screenOldEdge.y - screenPivot.y);
        const pivotToEvent = new Vector(screenEventMarker.x - screenPivot.x, screenEventMarker.y - screenPivot.y);
        if (marker.direction == 'x') {
            this.scaleFromFactors(pivotToEvent.length() / pivotToOld.length(), 1, this.pivot_.getPosition());
        } else {
            this.scaleFromFactors(1, pivotToEvent.length() / pivotToOld.length(), this.pivot_.getPosition());
        }
        } else {
        const screenOppositeEdge = projection.fromLatLngToContainerPixel(edges[(index+2)%4]);
        const oldDistance = Math.hypot(screenOldEdge.x - screenOppositeEdge.x, screenOldEdge.y - screenOppositeEdge.y);
        const newDistance = Math.hypot(screenEventMarker.x - screenOppositeEdge.x, screenEventMarker.y - screenOppositeEdge.y);
        this.imageRatioToWorld[marker.direction] = this.imageRatioToWorld[marker.direction] * newDistance / oldDistance;
        if (marker.direction == 'x') {
            const newScreenCenter = new google.maps.Point(
                screenCenter.x + marker.sign * Math.cos(this.angle_)*(oldDistance - newDistance)/2,
                screenCenter.y + marker.sign * Math.sin(this.angle_)*(oldDistance - newDistance)/2);
            this.center_ = projection.fromContainerPixelToLatLng(newScreenCenter);
        } else {
            const newScreenCenter = new google.maps.Point(
                screenCenter.x - marker.sign * Math.sin(this.angle_)*(oldDistance - newDistance)/2,
                screenCenter.y + marker.sign * Math.cos(this.angle_)*(oldDistance - newDistance)/2);
            this.center_ = projection.fromContainerPixelToLatLng(newScreenCenter);
        }
        }
        this.updatePlacemarkers();
    }

    drag(event) {
        const newPath = this.clickArea_.getPath().getArray();
        // newPath.forEach((np,i) => {
        //   this.testers[1].setPosition(np)
        //   this.testers[1].setMap(this.map_)
        // })
        const oldCorners = this.getCorners();
        this.center_ = new google.maps.LatLng(
            this.center_.lat() + newPath[0].lat() - oldCorners[0].lat(),
            this.center_.lng() + newPath[0].lng() - oldCorners[0].lng(),
        );
        this.pivot_.setPosition(new google.maps.LatLng(
            this.pivot_.getPosition().lat() + newPath[0].lat() - oldCorners[0].lat(),
            this.pivot_.getPosition().lng() + newPath[0].lng() - oldCorners[0].lng(),
        ));
        this.updatePlacemarkers();
    }

    updatePlacemarkers() {
        // Update the location of the edge placemarkers
        const edges = this.getEdges();
        this.edges_.forEach((m, i) => {
        m.setPosition(edges[i]);
        m.setZIndex(2);
        m.setCursor(this.getScaleCursor(edges[i]));
        });
        // Update the location of the corner placemarkers
        const corners = this.getCorners();
        this.rotators_.forEach((m, i) => {
        m.setPosition(corners[i]);
        m.setZIndex(1);
        m.setCursor(this.getRotatorCursor(corners[i]));
        });
        this.clickArea_.setPath(corners);
        this.corners_.forEach((m, i) => {
        m.setPosition(corners[i]);
        m.setZIndex(1);
        m.setCursor(this.getScaleCursor(corners[i]));
        });
    }

    getImageSizeInPixels() {
        const worldWidth = this.getProjection().getWorldWidth();
        return new google.maps.Point(this.imageRatioToWorld.x * worldWidth, this.imageRatioToWorld.y * worldWidth);
    }

    getCorners() {
        const center = this.center_;
        const projection = this.getProjection();
        const centerInPixels = projection.fromLatLngToContainerPixel(center);
        const imageSize = this.getImageSizeInPixels();

        return [
        projection.fromContainerPixelToLatLng(this.computeOffset(centerInPixels, imageSize.x/2, -imageSize.y/2)), //sw
        projection.fromContainerPixelToLatLng(this.computeOffset(centerInPixels, -imageSize.x/2, -imageSize.y/2)), //nw
        projection.fromContainerPixelToLatLng(this.computeOffset(centerInPixels, -imageSize.x/2, imageSize.y/2)), //ne
        projection.fromContainerPixelToLatLng(this.computeOffset(centerInPixels, imageSize.x/2, imageSize.y/2)), //se
        ];
    }

    getCornersInPixels() {
        const center = this.center_;
        const projection = this.getProjection();
        const centerInPixels = projection.fromLatLngToContainerPixel(center);
        const imageSize = this.getImageSizeInPixels();

        return [
        this.computeOffset(centerInPixels, imageSize.x/2, -imageSize.y/2), //sw
        this.computeOffset(centerInPixels, -imageSize.x/2, -imageSize.y/2), //nw
        this.computeOffset(centerInPixels, -imageSize.x/2, imageSize.y/2), //ne
        this.computeOffset(centerInPixels, imageSize.x/2, imageSize.y/2), //se
        ];
    }
    getBounds() {
        const center = this.center_;
        const projection = this.getProjection();
        const centerInPixels = projection.fromLatLngToContainerPixel(center);
        const imageSize = this.getImageSizeInPixels();

        const sw = projection.fromContainerPixelToLatLng(new google.maps.Point(centerInPixels.x - imageSize.x/2, centerInPixels.y + imageSize.y/2));
        const ne = projection.fromContainerPixelToLatLng(new google.maps.Point(centerInPixels.x + imageSize.x/2, centerInPixels.y - imageSize.y/2));
        return new google.maps.LatLngBounds(sw, ne);
    }
    // Helper function to calculate the location of a new point given x and y distance and a rotation
    computeOffset(start, horizontalDistance, verticalDistance) {
        const x = start.x + horizontalDistance * Math.cos(this.angle_) - verticalDistance * Math.sin(this.angle_);
        const y = start.y + horizontalDistance * Math.sin(this.angle_) + verticalDistance * Math.cos(this.angle_);
        return new google.maps.Point(x, y);
    }

    // Get the locations for each of the edge placemarkers
    getEdges() {
        const corners = this.getCorners();
        const edges = [];
        for (let i = 0; i < corners.length; i++) {
        edges[i] = new google.maps.LatLng(
            (corners[i].lat() + corners[(i+1)%4].lat()) / 2,
            (corners[i].lng() + corners[(i+1)%4].lng()) / 2,
        );
        }
        return edges;
    }

    // Get the correct cursor for mousing over a placemarker depending on its location relative
    // to the center
    getScaleCursor(cursorLocation) {
        const heading = google.maps.geometry.spherical.computeHeading(cursorLocation, this.center_);
        if (heading < -157.5) {
        return 'n-resize';
        } if (heading < -112.5) {
        return 'ne-resize';
        } if (heading < -67.5) {
        return 'e-resize';
        } if (heading < -22.5) {
        return 'se-resize';
        } if (heading < 22.5) {
        return 's-resize';
        } if (heading < 67.5) {
        return 'sw-resize';
        } if (heading < 112.5) {
        return 'w-resize';
        } if (heading < 157.5) {
        return 'nw-resize';
        }
        return 'n-resize';
    }

    // Get the correct cursor for mousing over a placemarker depending on its location relative
    // to the center
    getRotatorCursor(cursorLocation) {
        const heading = google.maps.geometry.spherical.computeHeading(cursorLocation, this.center_);
        if (heading < -90) {
        return 'url(\'../images/ne-rotate.png\') 7.5 7.5, auto';
        } if (heading < 0) {
        return 'url(\'../images/se-rotate.png\') 7.5 7.5, auto';
        } if (heading < 90) {
        return 'url(\'../images/sw-rotate.png\') 7.5 7.5, auto';
        }
        return 'url(\'../images/nw-rotate.png\') 7.5 7.5, auto';
    }

    addParcelOutline(parcel) {
        const projection = this.getProjection();
        parcel.setOptions({
        fillColor: 'rgb(144,238,144)',
        strokeColor: 'rgb(200,0,0)',
        strokeWeight: document.editableParcels ? 3 : 0,
        editable: document.editableParcels});
        parcel.setMap(this.map);
        parcel.index = document.parcelPolygons.length;

        google.maps.event.addListener(parcel, 'click', (e) => {
          parcel.setMap(null);
          document.parcelPolygons.splice(parcel.index, 1);
          this.draw()
        });
        document.parcelPolygons.push(parcel);
        this.draw();
    }

    updateImageEdit() {
        if (document.imageIsEditable) {
          this.edges_.forEach((m) => {
              m.setMap(null);
          });
          this.rotators_.forEach((m) => {
              m.setMap(null);
          });
          this.clickArea_.setMap(null);
          this.corners_.forEach((m) => {
              m.setMap(null);
          });
          this.pivot_.setMap(null);
        } else {
          this.edges_.forEach((m) => {
              m.setMap(this.map_);
          });
          this.rotators_.forEach((m) => {
              m.setMap(this.map_);
          });
          this.clickArea_.setMap(this.map_);
          this.corners_.forEach((m) => {
              m.setMap(this.map_);
          });
          if(this.hasPivot){
            this.pivot_.setMap(this.map_);
          }
        }
    }

    updateClipping() {
        if (!document.applyClipping) {
          this.img_.style.clipPath = '';
        } else {
          this.img_.style.clipPath = 'url(#svgClip' + this.imageOverlayIndex + ')';
        }
    }

    updateEditableParcels() {
        if (!document.editableParcels) {
          document.parcelPolygons.forEach((pp)=> {
              pp.setOptions({strokeWeight: 0, editable: false});
          });
        } else {
          document.parcelPolygons.forEach((pp)=> {
              pp.setOptions({strokeWeight: 3, editable: true});
          });
        }
    }
    
    downloadKMZ() {
      let imageForDownload = null;
      if (document.applyClipping) {
        // Convert SVG to create image mask
        const maskSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        const imageSize = this.getImageSizeInPixels();
        const projection = this.getProjection();
        const centerInPixels = projection.fromLatLngToDivPixel(this.center_);

        document.parcelPolygons.forEach((parcel, index) => {
          const path = parcel.getPath().getArray();
          let pathString = '';
          path.forEach((p, i) => {
          // Get the location of each point on the parcel outline relative to the 
          // image bounding box
          const point = projection.fromLatLngToDivPixel(p);
          const pathPoint = new google.maps.Point(
              point.x - (centerInPixels.x - imageSize.x/2),
              point.y - (centerInPixels.y - imageSize.y/2));
          // Rotate the location of the points on the parcel outline to account for the rotation of the image
          const rotatedPathPoint = new google.maps.Point(
              imageSize.x/2 + (pathPoint.x - imageSize.x/2) * Math.cos(-this.angle_) - (pathPoint.y - imageSize.y/2) * Math.sin(-this.angle_),
              imageSize.y/2 + (pathPoint.x - imageSize.x/2) * Math.sin(-this.angle_) + (pathPoint.y - imageSize.y/2) * Math.cos(-this.angle_));
          pathString += `${i == 0 ? 'M' : 'L'} ${rotatedPathPoint.x}, ${rotatedPathPoint.y} `;
          });
          pathString += 'Z';
          const svgPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
          svgPath.setAttribute('d', pathString);
          svgPath.style.strokeWidth = 0;
          svgPath.style.fill = 'white';
          maskSVG.appendChild(svgPath);
        });

        // Create image mask from SVG
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        ctx.canvas.width = imageSize.x;
        ctx.canvas.height = imageSize.y;
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        const data = (new XMLSerializer()).serializeToString(maskSVG);
        const DOMURL = window.URL || window.webkitURL || window;

        const img = new Image();
        const svgBlob = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
        const url = DOMURL.createObjectURL(svgBlob);

        const overlay = this; //pass the overlay "this" into the context where "this" would be the img
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            DOMURL.revokeObjectURL(url);

            const imgURI = canvas
                .toDataURL('image/png')
                .replace('image/png', 'image/octet-stream');

            Jimp.read(Buffer.from(imgURI.split(',')[1], 'base64'))
                .then((mask) => {
                Jimp.read(Buffer.from(overlay.image_.split(',')[1], 'base64'))
                    .then((image) => {
                        image.mask(mask.resize(overlay.imageWidth, overlay.imageHeight), 0, 0)
                            .getBase64Async(Jimp.MIME_PNG)
                            .then((outImg) => {
                              overlay.createKMZ(outImg);
                            })
                            .catch((err) => {
                            console.error(err);
                            });
                    })
                    .catch((err) => {
                        console.error(err);
                    });
                })
                .catch((err) => {
                console.error(err);
                });
        };
        img.src = url; //Set the src for the image, this will trigger img.onload, but the img.onload pointer needs to be defined first to make sure it triggers
      } else {
        this.createKMZ(this.image_);
        document.controlText.innerHTML = 'Download KMZ';
      }
    }
    createKMZ(downloadImage) {
      var imageName = this.imageName
      const zip = new JSZip();
      let overlayKMLString = "";
      zip.folder('files').file(this.imageName, downloadImage.split('base64,')[1], {base64: true});
      const north = this.getBounds().getNorthEast().lat();
      const south = this.getBounds().getSouthWest().lat();
      const east = this.getBounds().getNorthEast().lng();
      const west = this.getBounds().getSouthWest().lng();
      const rotation = -this.angle_ * 180 / Math.PI;
      const imageFileName = this.imageName;
      const overlayName = this.imageName.split('.')[0];
      overlayKMLString += `<GroundOverlay>`+
      `<name>${overlayName}</name>`+
      `<Icon>`+
          `<href>files/${imageFileName}</href>`+
          `<viewBoundScale>0.75</viewBoundScale>`+
      `</Icon>`+
      `<LatLonBox>`+
          `<north>${north}</north>`+
          `<south>${south}</south>`+
          `<east>${east}</east>`+
          `<west>${west}</west>`+
          `<rotation>${rotation}</rotation>`+
      `</LatLonBox>`+
      `</GroundOverlay>`;

      const kmlTemplate = `<?xml version="1.0" encoding="UTF-8"?>` +
      `<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">` +
      `<Document>` +
      overlayKMLString + 
      `</Document>` + 
      `</kml>`;
      zip.file('doc.kml', kmlTemplate);

      zip.generateAsync({type: 'blob'}).then(function(content) {
        saveAs(content,imageName.split('.')[0] + '.kmz');
      }, function(err) {
        console.log(err);
      })
    }
  }
};

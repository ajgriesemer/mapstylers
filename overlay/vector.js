// Simple vector library from https://geekrodion.medium.com/linear-algebra-vectors-f7610e9a0f23
class Vector {
  constructor(...components) {
    this.components = components;
  }
  scaleBy(number) {
    return new Vector(
        ...this.components.map((component) => component * number),
    );
  }

  angleBetween(other) {
    return toDegrees(
        Math.acos(
            this.dotProduct(other) /
        (this.length() * other.length()),
        ),
    );
  }

  length() {
    return Math.hypot(...this.components);
  }

  dotProduct({components}) {
    return components.reduce((acc, component, index) => acc + component * this.components[index], 0);
  }
  normalize() {
    return this.scaleBy(1 / this.length());
  }
  projectOn(other) {
    const normalized = other.normalize();
    return normalized.scaleBy(this.dotProduct(normalized));
  }
  projectOnAngle(angle) {
    const vector = new Vector(Math.cos(angle), Math.sin(angle));
    return vector.scaleBy(this.dotProduct(vector));
  }
}

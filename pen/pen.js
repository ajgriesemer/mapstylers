let map;
document.drawingEnabled = false;
document.isDrawing = false; 

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 39.167747, lng: -94.636855 },
    zoom: 15,
    styles: style,
    mapTypeId: "hybrid",
    gestureHandling: "greedy" //For mobile, this allows the map to drag on a swipe instead of scroll
  });
  function onMove(e) {
    if(document.isDrawing && document.drawingEnabled){
      if(e.domEvent.buttons == 0) // Check mouse event to double check if left button is not pressed
      {
        console.log("Caught missed mouseup")
        map.setOptions({gestureHandling: "greedy"}); //turn on dragging and scrolling
        document.isDrawing = false;
      } else {
        var path = document.polyline.getPath()
        path.push(e.latLng)
        document.polyline.setPath(path)
      }
    }
  }
  map.addListener("mousemove", onMove);
  map.addListener("touchmove", onMove);
  map.addListener("mousedown", function(e) {
    
    if(document.drawingEnabled && e.domEvent.which == 1){ // If in pen drawing mode and the left mouse button has been clicked
      map.setOptions({gestureHandling: "none"}); // Turn off dragging, this will also turn off scrolling
      document.isDrawing = true;
      document.polyline = new google.maps.Polyline({
        path: [e.latLng],
        geodesic: false,
        clickable: false, //If the polyline receives click events, sometimes it causes the map to drag
        strokeColor: "#17B5FD",
        strokeOpacity: 1.0,
        strokeWeight: 4,
        map: map
      });
      console.log("mousedown")
    }
  });
  map.addListener("mouseup", function(e) {
    // Check that the mouseevent is for the primary (left) mouse button
    if(e.domEvent.which == 1){
      map.setOptions({gestureHandling: "greedy"}); //turn on dragging and scrolling
      document.isDrawing = false;
      console.log("mouseup")
    }
  });

  // Create the DIV to hold the control and call the CenterControl()
  // constructor passing in this DIV.
  const centerControlDiv = document.createElement("div");

  CenterControl(centerControlDiv, map);
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
}


function CenterControl(controlDiv, map) {
  const controlUI = document.createElement("div");

  controlUI.style.backgroundColor = "#fff";
  controlUI.style.border = "2px solid #fff";
  controlUI.style.borderRadius = "3px";
  controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
  controlUI.style.cursor = "pointer";
  controlUI.style.marginTop = "8px";
  controlUI.style.marginBottom = "22px";
  controlUI.style.textAlign = "center";
  controlUI.title = "Click to recenter the map";
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  const controlText = document.createElement("div");

  controlText.style.color = "rgb(25,25,25)";
  controlText.style.fontFamily = "Roboto,Arial,sans-serif";
  controlText.style.fontSize = "16px";
  controlText.style.lineHeight = "38px";
  controlText.style.paddingLeft = "5px";
  controlText.style.paddingRight = "5px";
  controlText.innerHTML = "Start Drawing";
  controlUI.appendChild(controlText);
  // Setup the click event listeners: simply set the map to Chicago.
  controlUI.addEventListener("click", () => {
    if(document.drawingEnabled){
      document.drawingEnabled = false;
      controlText.innerHTML = "Start Drawing"
    } else {
      document.drawingEnabled = true;
      controlText.innerHTML = "Stop Drawing"
    }
  });
}
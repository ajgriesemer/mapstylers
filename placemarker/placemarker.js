vm1 = new Vue({
  el: "#app",
  data: function() {
    return {
      markers: []
    };
  },
  methods: {
    deleteMarker: function(id) {
      console.log(`deleting ${id}`);
      console.log(this.markers);
      for (let j = 0; j < this.markers.length; j++) {
        const element = this.markers[j];
        if (element.id == id) {
          this.markers.splice(j, 1);
        }
      }
      console.log(this.markers);
    }
  },
  watch: {
    markers: {
      handler: function(newdata, old) {
        for (let i = 0; i < newdata.length; i++) {
          if (!document.markers) {
            document.markers = [];
          }
          if (document.markers[i]) {
            document.getElementById("testText").innerHTML =
              newdata[i].label.text;
            textWidth = document.getElementById("testTextDiv").offsetWidth;
            flagWidth = textWidth + 25;
            newdata[i].icon.labelOrigin.x = 15 + textWidth / 2;
            newdata[i].icon.path =
              "M 0 10 L 10 20 L " +
              flagWidth.toString() +
              " 20 L " +
              flagWidth.toString() +
              " 0 L 10 0 Z";

            if (newdata[i].label.text == "") {
              newdata[i].label.text = " ";
            }

            document.markers[i].setIcon(newdata[i].icon);
            document.markers[i].setLabel(newdata[i].label);
            document.markers[i].setPosition(newdata[i].latLng);
          } else {
            document.markers.push(
              new google.maps.Marker({
                position: newdata[i].latLng,
                map: map,
                label: newdata[i].label,
                icon: newdata[i].icon,
                draggable: true
              })
            );
          }
        }
        if (document.markers.length > newdata.length) {
          for (let k = newdata.length; k < document.markers.length; k++) {
            const element = document.markers[k];
            element.setMap(null);
            document.markers.splice(k, 1);
          }
        }
      },
      deep: true
    }
  }
});

var map;
var start = "";
var end = "";
var markers = [];

function initMap() {
  document.overlay = new google.maps.OverlayView();
  document.directionsService = new google.maps.DirectionsService();
  document.directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: true,
    markerOptions: {
      // visible:false
    },
    polylineOptions: {
      strokeColor: "red"
    }
  });
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 39.167747, lng: -94.636855 },
    zoom: 15,
    styles: style,
    mapTypeId: "hybrid"
  });
  document.directionsDisplay.setMap(map);

  map.addListener("click", function(e) {
    vm1.$data.markers.push({
      label: {
        text: "Site Name",
        fontFamily: "FolioStd-ExtraBold",
        color: "white"
      },
      id: Math.random(),
      latLng: e.latLng,
      icon: {
        path: "M 0 10 L 10 20 L 105 20 L 105 0 L 10 0 Z",
        anchor: { x: 0.0, y: 10 },
        fillColor: "orange",
        fillOpacity: 1.0,
        strokeWeight: 0,
        labelOrigin: { x: 55, y: 11.5 }
      }
    });
  });
}

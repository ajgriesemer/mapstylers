
function initMap() {
  const dropArea = document.getElementById('dropzone');

  ['dragenter', 'dragover', 'dragleave', 'drop'].forEach((eventName) => {
    dropArea.addEventListener(eventName, preventDefaults, false);
  });

  function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
  }
  ['dragenter', 'dragover'].forEach((eventName) => {
    document.addEventListener(eventName, highlight, false);
  });
  ['drop'].forEach((eventName) => {
    document.addEventListener(eventName, loadDropped, false);
    dropArea.addEventListener(eventName, loadDropped, false);
  });

  function highlight(e) {
    if (!document.imageLoaded) {
      dropArea.classList.add('highlight');
    }
  }

  function loadDropped(e) {
    dropArea.classList.remove('highlight');
    if (!document.imageLoaded) {
      const reader = new FileReader();
      const fileName = e.dataTransfer.files[0].name;
      reader.readAsText(e.dataTransfer.files[0]);
      reader.onloadend = function() {
        initImage(map, reader.result, fileName);
      };
    }
  }
  const mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(39.11, -94.73),
    mapTypeId: 'hybrid',
    tilt: 0,
    styles: [
      {
        'featureType': 'poi',
        'stylers': [
          {
            'visibility': 'off',
          },
        ],
      },
    ],
  };
  const map = new google.maps.Map(document.getElementById('map'), mapOptions);
  REP.Layer.Google.Initialize(map);

  async function initImage(map, kml, name) {     
    //https://drive.google.com/file/d/19H4vcKOUyC_I33AJbH2EQKqbWYg5iuV3/view?usp=sharing
    //https://drive.google.com/file/d/1Q53HDOSoSuCjIaaPgcCrjyJdIJDs1PCV/view?usp=sharing
    //https://drive.google.com/file/d/1Oy4IsGdnqIODUk_WryF26RVd2EgpAouZ/view?usp=sharing
    var kmlLayer = new google.maps.KmlLayer("https://drive.google.com/uc?export=download&id=1Q53HDOSoSuCjIaaPgcCrjyJdIJDs1PCV", {
      suppressInfoWindows: true,
      preserveViewport: false,
      map: map
    });
    
  }
}

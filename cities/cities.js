// Lower resolution boundaries: https://www.census.gov/geographies/mapping-files/time-series/geo/cartographic-boundary.html
// Highest resolution boundaries: https://www.census.gov/geographies/mapping-files/time-series/geo/tiger-line-file.html
// Esri Shapefile to Geojson converter https://mapshaper.org/
//    Github repo https://github.com/mbloch/mapshaper
// Sources file for Geojson to Google Maps converter https://github.com/JasonSanford/geojson-google-maps
// Definition of description fields https://www.census.gov/programs-surveys/geography/technical-documentation/records-layout/nlt-record-layouts.html

function initMap() {
  
  const mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(38.928, -93.122),
    mapTypeId: 'hybrid',
    tilt: 0,
    styles: [
      {
        'featureType': 'poi',
        'stylers': [
          {
            'visibility': 'off',
          },
        ],
      },
    ],
  };
  const map = new google.maps.Map(document.getElementById('map'), mapOptions);
  tl_2021_29_place.features.forEach(county => {
    if(county.geometry.type == 'MultiPolygon'){
      var myGoogleVector = new GeoJSON(county);
      
      myGoogleVector.forEach(polygon => polygon.setMap(map))   
    }
    else{
      var polygon = new GeoJSON(county);
      
      google.maps.event.addListener(polygon, 'click', (e) => {
        polygon.setOptions({
          strokeColor: '#afd'
        });
        const infowindow = new google.maps.InfoWindow({
          content: JSON.stringify(county.properties),
          position:e.latLng
        });
        infowindow.open({
          map,
          shouldFocus: false,
        });
      });
      
      polygon.setMap(map);   
    } 
  })
}

var circle = {
  path: 'M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
  fillOpacity: 0.5,
  strokeWeight: 2,
  scale: 0.1
};
var bigger_circle = {
  path: 'M 0, 0 m -150, 0 a 150,150 0 2,0 300,0 a 150,150 0 2,0 -300,0',
  fillOpacity: 0.5,
  fillOpacity: 0,
  strokeWeight: 2,
  scale: 0.1
};
var pivot = {
  path: 'M 0, 0 m -75, 0 a 75,75 0 1,0 150,0 a 75,75 0 1,0 -150,0',
  fillOpacity: 1,
  strokeWeight: 2,
  scale: 0.1,
  fillColor: 'white'
};


// document.addEventListener("dragenter", function(event) {
//   console.log('dragged')
//   document.getElementById("image-upload").style.borderColor = "#4d90fe"
// });

// document.addEventListener('load', function() {
//   document.querySelector('input[type="file"]').addEventListener('change', function() {
//       if (this.files && this.files[0]) {
//           var img = document.querySelector('img');  // $('img')[0]
//           img.src = URL.createObjectURL(this.files[0]); // set src to blob url
//           img.onload = imageIsLoaded;
//           console.log(img)
//       }
//   });
// });

// Dropzone.options.imageUpload = {
// }


function initMap() {
  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(39.11,-94.73),
    mapTypeId: 'hybrid'
  };
  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  var image_src = '../images/Turner Concept Plan.png';
  var image_height = 642;
  var image_width = 1001;

  class DebugOverlay extends google.maps.OverlayView {
    constructor(image, map, image_width, image_height) {
      super();
      this.image_ = image;
      this.image_width_ = image_width;
      this.image_height_ = image_height;
      this.map_ = map;
      this.div_ = null;
      this.bounds_ = null;
      this.angle_ = 0;
      this.setMap(map);
      this.hasPivot = true;
    }
    
    onAdd() {
      var overlayProjection = this.getProjection();

      var div = document.createElement('div');
      div.style.borderStyle = 'solid';
      div.style.borderWidth = '1px';
      div.style.position = 'absolute';
      var img = document.createElement('img');
      img.src = this.image_;
      img.style.width = '100%';
      img.style.height = '100%';
      img.style.position = 'absolute';
      this.div_ = div;
      this.img_ = img;
      div.appendChild(img);
      var panes = this.getPanes();
      panes.overlayLayer.appendChild(div);

      var map_bounds = this.map_.getBounds();
      var map_center = this.map_.getCenter();
      this.bounds_ = new google.maps.LatLngBounds(
        new google.maps.LatLng(
          map_center.lat() - ((map_bounds.getNorthEast().lat() - map_bounds.getSouthWest().lat()) / document.getElementById("map").offsetHeight * this.image_height_)/2,
          map_center.lng() - ((map_bounds.getNorthEast().lng() - map_bounds.getSouthWest().lng()) / document.getElementById("map").offsetWidth * this.image_width_)/2
        ),
        new google.maps.LatLng(
          map_center.lat() + ((map_bounds.getNorthEast().lat() - map_bounds.getSouthWest().lat()) / document.getElementById("map").offsetHeight * this.image_height_)/2,
          map_center.lng() + ((map_bounds.getNorthEast().lng() - map_bounds.getSouthWest().lng()) / document.getElementById("map").offsetWidth * this.image_width_)/2
        ),
      );
      var colors = ['red','green','blue', 'yellow']
      this.rotators_ = this.getCorners().map((c,i) => {
        var colored_circle = bigger_circle;
        colored_circle.strokeColor = colors[i];
        var marker = new google.maps.Marker({
          position: c,
          map: map,
          draggable:true,
          icon: colored_circle,
          zIndex: 1
        });
        google.maps.event.addListener(marker,'drag', (e) => {this.rotate(e,i)});
        google.maps.event.addListener(marker,'dragend', (e) => {this.rotate(e,i)});
        return marker
      })
      this.corners_ = this.getCorners().map((c,i) => {
        var colored_circle = circle;
        colored_circle.strokeColor = colors[i];
        var marker = new google.maps.Marker({
          position: c,
          map: map,
          draggable:true,
          icon: colored_circle,
          zIndex: 2
        });
        google.maps.event.addListener(marker,'drag', (e) => {this.scale(e,i)});
        google.maps.event.addListener(marker,'dragend', (e) => {this.scale(e,i)});
        return marker
      })
      this.edges_ = this.getEdges().map((c,i) => {
        var colored_circle = circle;
        colored_circle.strokeColor = colors[i];
        var marker = new google.maps.Marker({
          position: c,
          map: map,
          draggable:true,
          icon: colored_circle,
          zIndex: 2
        });
        google.maps.event.addListener(marker,'drag', (e) => {this.edgeScale(e,i)});
        google.maps.event.addListener(marker,'dragend', (e) => {this.edgeScale(e,i)});
        return marker
      })

      this.path = new google.maps.Polyline()

      this.rectangle = new google.maps.Rectangle()

      this.pivot_location_ = this.getCenter()
      this.pivot_ = new google.maps.Marker({
        position: this.getCenter(),
        map: map,
        draggable:true,
        icon: pivot
      });
      this.adjusted_pivot_ = new google.maps.Marker();
      google.maps.event.addListener(this.pivot_,'dragend', (e) => {this.updatePivot(e)});
      google.maps.event.addListener(this.pivot_,'rightclick', () => {
        this.pivot_.setMap(null);
        this.hasPivot = false;
      });

      this.map_.addListener("click", (e) => {
        if(!this.hasPivot){
          this.pivot_.setPosition(e.latLng)
          this.pivot_.setMap(this.map_);
          this.hasPivot = true;
        }
      })
      this.accumulated_error_lat = 0;
      this.accumulated_error_lng = 0;
      this.offset_lat_ = 0;
      this.offset_lng_ = 0;
    };
    draw() {
      var overlayProjection = this.getProjection();
      var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
      var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());
      var pivot = overlayProjection.fromLatLngToDivPixel(this.pivot_.getPosition());
      
      // console.log([this.bounds_.getSouthWest(),this.bounds_.getNorthEast()].reduce((a,c)=> {
      //   return a.concat([c.lat(), c.lng()])
      // },[]))
      var div = this.div_;
      //this.div_.style.transformOrigin = (pivot.x - sw.x) + 'px ' + (pivot.y - ne.y) + 'px';
      this.div_.style.transform = 'rotate('+this.angle_+'deg)';
      div.style.left = sw.x + 'px';
      div.style.top = ne.y + 'px';
      div.style.width = (ne.x - sw.x) + 'px';
      div.style.height = (sw.y - ne.y) + 'px';
      
      this.rectangle.setOptions({
        bounds: this.bounds_,
        strokeColor: "#FF0000",
        strokeWeight: 2,
        map: this.map_
      })
      
    };

    rotate(event,index) {
      // update angle
      var old_angle = google.maps.geometry.spherical.computeHeading(this.getCorners()[index], this.getCenter());
      var new_angle = google.maps.geometry.spherical.computeHeading(event.latLng, this.getCenter());
      this.angle_ += new_angle - old_angle
      
      // translate bounds to account for rotation.
      var mapProjection = map.getProjection();
      var pivot_point = mapProjection.fromLatLngToPoint(this.pivot_.getPosition());
      var center_point = mapProjection.fromLatLngToPoint(this.getCenter());
      var angleRad = (new_angle - old_angle) * Math.PI / 180.0;
      var translated_pivot_point = {
        x: Math.cos(angleRad) * (pivot_point.x - center_point.x) - Math.sin(angleRad) * (pivot_point.y - center_point.y) + center_point.x,
        y: Math.sin(angleRad) * (pivot_point.x - center_point.x) + Math.cos(angleRad) * (pivot_point.y - center_point.y) + center_point.y
      }
      var translated_pivot = mapProjection.fromPointToLatLng(translated_pivot_point);
      var adj_distance = google.maps.geometry.spherical.computeDistanceBetween(this.pivot_.getPosition(),translated_pivot);
      var adj_heading = google.maps.geometry.spherical.computeHeading(this.pivot_.getPosition(),translated_pivot);

      var old_sw = this.bounds_.getSouthWest()
      var new_sw = google.maps.geometry.spherical.computeOffset(this.bounds_.getSouthWest(),-adj_distance,adj_heading)
      
      // var old_ne = this.bounds_.getNorthEast()
      // var new_ne = google.maps.geometry.spherical.computeOffset(this.bounds_.getNorthEast(),-adj_distance,adj_heading)

      // var sw_offset = {lat: new_sw.lat() - old_sw.lat(), lng: new_sw.lng() - old_sw.lng()}
      // var ne_offset = {lat: new_ne.lat() - old_ne.lat(), lng: new_ne.lng() - old_ne.lng()}
      // this.accumulated_error_lat += ne_offset.lat - sw_offset.lat
      // this.accumulated_error_lng += ne_offset.lng - sw_offset.lng

      // console.log(this.accumulated_error_lat.toFixed(20), this.accumulated_error_lng.toFixed(20))
      // //console.log(ne_offset.lat - sw_offset.lat, ne_offset.lng - sw_offset.lng);
      this.offset_lat_ += old_sw.lat() - new_sw.lat()
      this.offset_lng_ += old_sw.lng() - new_sw.lng()
      this.bounds_ = new google.maps.LatLngBounds(
        //southwest
        google.maps.geometry.spherical.computeOffset(this.bounds_.getSouthWest(),-adj_distance,adj_heading), 
        //northeast
        google.maps.geometry.spherical.computeOffset(this.bounds_.getNorthEast(),-adj_distance,adj_heading)
      );

      var bounds_pivot = new google.maps.LatLng(this.pivot_.getPosition().lat() - this.offset_lat_, this.pivot_.getPosition().lng() - this.offset_lng_)
      
      this.adjusted_pivot_.setOptions({
        position: bounds_pivot,
        map: map,
        draggable:true
      })
      // console.log([this.bounds_.getSouthWest(),this.bounds_.getNorthEast()].reduce((a,c)=> {
      //   return a.concat([c.lat(), c.lng()])
      // },[]))
      this.draw();
      this.positionMarkers();
    }

    scale(event,index) {
      if(this.hasPivot){
        var pivot = this.pivot_
      }
      else{
        var pivot = this.corners_[(index+2)%4];
      }
      var old_distance = google.maps.geometry.spherical.computeDistanceBetween(this.getCorners()[index], pivot.getPosition());
      var new_distance = google.maps.geometry.spherical.computeDistanceBetween(event.latLng, pivot.getPosition());
      
      var sw_heading_end = google.maps.geometry.spherical.computeHeading(pivot.getPosition(), this.getCorners()[0]);
      var sw_distance = google.maps.geometry.spherical.computeDistanceBetween(this.getCorners()[0], pivot.getPosition());

      var ne_heading_end = google.maps.geometry.spherical.computeHeading(pivot.getPosition(), this.getCorners()[2]);
      var ne_distance = google.maps.geometry.spherical.computeDistanceBetween(this.getCorners()[2], pivot.getPosition());

      ///find equivalent to pivot in bounds
      //var bounds_pivot = google.maps.geometry.spherical.computeOffset(this.bounds_.getSouthWest(), sw_distance, sw_heading - this.angle_)
      var bounds_pivot = new google.maps.LatLng(pivot.getPosition().lat() - this.offset_lat_, pivot.getPosition().lng() - this.offset_lng_)
      
      var sw_scaled = google.maps.geometry.spherical.computeOffset(bounds_pivot,sw_distance * new_distance / old_distance,sw_heading_end - this.angle_)
      var ne_scaled = google.maps.geometry.spherical.computeOffset(bounds_pivot,ne_distance * new_distance/ old_distance,ne_heading_end - this.angle_)
      
      var pivots_heading = google.maps.geometry.spherical.computeHeading(bounds_pivot, pivot.getPosition());
      var pivots_distance  = google.maps.geometry.spherical.computeDistanceBetween(bounds_pivot, pivot.getPosition());
      var pivots_nudged = google.maps.geometry.spherical.computeOffset(bounds_pivot, pivots_distance * new_distance / old_distance,pivots_heading)
      var nudge_distance = {lat: pivots_nudged.lat()-pivot.getPosition().lat(),lng: pivots_nudged.lng()-pivot.getPosition().lng()}
      
      this.offset_lat_ += nudge_distance.lat
      this.offset_lng_ += nudge_distance.lng

      this.adjusted_pivot_.setOptions({
        position: bounds_pivot,
        map: map,
        draggable:true
      })

      this.bounds_ = new google.maps.LatLngBounds(
        //southwest
        new google.maps.LatLng(sw_scaled.lat() - nudge_distance.lat, sw_scaled.lng() - nudge_distance.lng ), 
        //northeast
        new google.maps.LatLng(ne_scaled.lat() - nudge_distance.lat, ne_scaled.lng() - nudge_distance.lng ),
      );
      // this.bounds_ = new google.maps.LatLngBounds(
      //   //southwest
      //   google.maps.geometry.spherical.computeOffset(bounds_pivot,sw_distance * new_distance / old_distance,sw_heading_end - this.angle_), 
      //   //northeast
      //   google.maps.geometry.spherical.computeOffset(bounds_pivot,ne_distance * new_distance/ old_distance,ne_heading_end - this.angle_)
      // );
      this.draw();
      this.positionMarkers();
      //this.corners_[0].setMap(null)
    }
    edgeScale(event, index){

    }
    updatePivot(event){
      var pivot_move_distance = google.maps.geometry.spherical.computeDistanceBetween(this.pivot_location_, event.latLng);
      var pivot_move_heading = google.maps.geometry.spherical.computeHeading(this.pivot_location_, event.latLng);

      var new_bounds_pivot = google.maps.geometry.spherical.computeOffset(this.pivot_location_,pivot_move_distance,pivot_move_heading - this.angle_)

      this.offset_lat_ += (event.latLng.lat() - new_bounds_pivot.lat())
      this.offset_lng_ += (event.latLng.lng() - new_bounds_pivot.lng())
      var bounds_pivot = new google.maps.LatLng(event.latLng.lat() - this.offset_lat_, event.latLng.lng() - this.offset_lng_)

      this.adjusted_pivot_.setOptions({
        position: bounds_pivot,
        map: map,
        draggable:true
      })
      this.pivot_location_ = event.latLng;
    }

    positionMarkers(){
      var corners = this.getCorners();
      this.rotators_.forEach((m,i) => {
        m.setPosition(corners[i])
      })
      this.corners_.forEach((m,i) => {
        m.setPosition(corners[i])
      })
      this.edges_.forEach((m,i) => {
        m.setPosition(this.getEdges()[i])
      })

    }
    getCorners(){
      var sw = this.bounds_.getSouthWest()
      var nw = new google.maps.LatLng(this.bounds_.getNorthEast().lat(), this.bounds_.getSouthWest().lng()) 
      var ne = this.bounds_.getNorthEast()
      var se = new google.maps.LatLng(this.bounds_.getSouthWest().lat(), this.bounds_.getNorthEast().lng())


      var corners = [sw, nw, ne, se]
      corners = corners.map((c) => {
        var distance = google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(),c);
        var heading = google.maps.geometry.spherical.computeHeading(this.getCenter(),c);
        return google.maps.geometry.spherical.computeOffset(this.getCenter(),distance,heading+this.angle_)
      });
      // this.accumulated_error_lat += sw.lat() - corners[0].lat()
      // this.accumulated_error_lng += sw.lng() - corners[0].lng()

      // console.log(this.accumulated_error_lat.toFixed(20), this.accumulated_error_lng.toFixed(20))
      return corners
    }
    getEdges(){
      var corners = this.getCorners();
      var w = new google.maps.LatLng((corners[0].lat() + corners[1].lat())/2, corners[0].lng())
      var n = new google.maps.LatLng(corners[1].lat(), (corners[1].lng() + corners[2].lng())/2)
      var e = new google.maps.LatLng((corners[2].lat() + corners[3].lat())/2, corners[2].lng())
      var s = new google.maps.LatLng(corners[3].lat(), (corners[0].lng() + corners[3].lng())/2)
      return [w, n, e, s]
    }
    getCenter(){
      return new google.maps.LatLng(
        (this.bounds_.getSouthWest().lat() + this.bounds_.getNorthEast().lat())/2,
        (this.bounds_.getSouthWest().lng() + this.bounds_.getNorthEast().lng())/2
        )
    }
    onRemove() {
      this.div_.parentNode.removeChild(this.div_);
      this.div_ = null;
    };
  }
  overlay = new DebugOverlay(image_src, map, image_width, image_height);

}
